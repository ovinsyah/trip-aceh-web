<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('admin/create/wisata','AdminWisataController@create_wisata');
Route::post('admin/update/wisata/{id}','AdminWisataController@update_wisata');
Route::get('admin/delete/wisata/{id}','AdminWisataController@delete_wisata');


Route::post('admin/create/akomodasi','AdminAkomodasiController@create_akomodasi');
Route::post('admin/update/akomodasi/{id}','AdminAkomodasiController@update_akomodasi');
Route::get('admin/delete/akomodasi/{id}','AdminAkomodasiController@delete_akomodasi');

Route::post('admin/create/ulasan/akomodasi','AdminAkomodasiController@create_ulasan_akomodasi');

Route::post('admin/create/budaya','AdminBudayaController@create_budaya');
Route::post('admin/update/budaya/{id}','AdminBudayaController@update_budaya');
Route::get('admin/delete/budaya/{id}','AdminBudayaController@delete_budaya');

Route::post('admin/create/slider','AdminSliderController@create_slider');
Route::post('admin/update/slider/{id}','AdminSliderController@update_slider');
Route::get('admin/delete/slider/{id}','AdminSliderController@delete_slider');

Route::get('admin/delete/user/{id}','AdminController@delete_user');


Route::post('user/update/ulasan/{id}','UserController@update_ulasan');


Route::post('/uji','AdminController@uji');
