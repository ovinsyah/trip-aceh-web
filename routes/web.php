<?php
use NumPHP\LinAlg\LinAlg;

Route::get('/c_Arr', function () {
	$matrix;
		$a=array(-1,1);
		for ($i=0; $i <10 ; $i++) { 
			for ($j=0; $j <10 ; $j++) { 
						$h=array(1,-1,($a[array_rand($a,1)]*round(rand()/getrandmax(),2)));
						$matrix[$i][$j]=$h[array_rand($h,1)];
					}		
		}
		return $matrix;
});
Route::get('/wisata_all', function () {
	$wisatas=App\Wisata::all();
	$all='';
	foreach ($wisatas as $key) {
		$all.=$key->judul.',';
	}
		$all=explode(",",$all);
    return explode(",",implode(',',array_unique($all)));
});

Auth::routes();
Route::get('admin/login','AuthAdmin\LoginController@showLoginForm');
Route::post('admin/login','AuthAdmin\LoginController@login');
Route::get('admin/register','AuthAdmin\RegisterController@showRegistrationForm');
Route::post('admin/register','AuthAdmin\RegisterController@register');

Route::get('admin/logout','AuthAdmin\LoginController@logout_admin');
Route::get('user/logout','Auth\LoginController@logout_user');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('user', function () {
   return redirect('/home');
});

/*================Public=====================*/
Route::get('/','PublicController@index');
Route::get('/tester','PublicController@tester');
Route::get('/backup','PublicController@backup');
Route::get('/wisata/','PublicWisataController@wisata');
Route::get('/recommendation-wisata/','PublicWisataController@recommendation');
Route::get('/top-rate-wisata/','PublicWisataController@toprate');
Route::get('/detail/wisata/{id}','PublicWisataController@wisatadetail');

Route::get('/akomodasi','PublicController@akomodasi');
Route::get('/detail/akomodasi/{id}','PublicController@akomodasidetail');

Route::get('/budaya','PublicController@budaya');
Route::get('/detail/budaya/{id}','PublicController@budayadetail');

Route::get('/detail/slider/{id}','PublicController@sliderdetail');
Route::get('/search/wisata/{key}','PublicController@search');

//filter
Route::get('/filter/akomodasi/{kota}','PublicController@filter_akomodasi');
Route::get('/filter/wisata/{kota}/{kategori}','PublicWisataController@filter_wisata');


/*================User=====================*/
Route::group(['prefix'=>'user','middleware'=>'auth:web'],function () {
	Route::get('/profile','UserController@index');
	Route::get('/edit/profile','UserController@edit');
	Route::post('/update/profile','UserController@update_profile');

	Route::get('/account','UserController@password');
	Route::post('/update/password','UserController@update_password');
});


/*================Admin=====================*/
Route::group(['prefix'=>'admin','middleware'=>'auth:admin'],function () {
	Route::get('/','AdminController@index');
	Route::get('/user','AdminController@user');

	Route::get('/wisata','AdminWisataController@wisata');
	Route::get('/add/wisata','AdminWisataController@addwisata');
	Route::get('/edit/wisata/{id}','AdminWisataController@editwisata');

	Route::get('/akomodasi','AdminAkomodasiController@akomodasi');
	Route::get('/add/akomodasi','AdminAkomodasiController@addakomodasi');
	Route::get('/edit/akomodasi/{id}','AdminAkomodasiController@editakomodasi');

	Route::get('/budaya','AdminBudayaController@budaya');
	Route::get('/add/budaya','AdminBudayaController@addbudaya');
	Route::get('/edit/budaya/{id}','AdminBudayaController@editbudaya');

	Route::get('/slider','AdminSliderController@slider');
	Route::get('/add/slider','AdminSliderController@addslider');
	Route::get('/edit/slider/{id}','AdminSliderController@editslider');

	Route::get('/ulasan','AdminController@ulasan');
	Route::get('/delete/ulasan/{id}','AdminController@delete_ulasan');
	Route::get('/pengujian','AdminController@pengujian');
	Route::get('/hasil-pengujian','AdminController@hasilpengujian');
	Route::get('/detail-pengujian/{id}','AdminController@detailhasilpengujian');
});

