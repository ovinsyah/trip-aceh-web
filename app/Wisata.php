<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wisata extends Model
{
     public function image()
    {
        return $this->hasMany(Image::class,'owner_id');
    }

     public function kota()
    {
        return $this->belongsTo(kota_aceh::class,'kota_aceh_id');
    }
     public function ulasan()
    {
        return $this->hasMany(Ulasan::class,'wisata_id');
    }
     public function akomodasi()
    {
        return $this->belongsToMany(Akomodasi::class,'akomodasi_wisatas');
    }
}
