<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budaya extends Model
{
     public function kota()
    {
        return $this->belongsTo(kota_aceh::class,'kota_aceh_id');
    }

     public function image()
    {
        return $this->hasMany(Image::class,'owner_id');
    }
}
