<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kota_aceh extends Model
{
	public function wisata()
	{
     return $this->hasMany('App\Wisata');
	}
	public function akomodasi()
	{
     return $this->hasMany('App\Akomodasi');
	}
	public function budaya()
	{
     return $this->hasMany('App\Budaya');
	}
}
