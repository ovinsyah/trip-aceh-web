<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UlasanAkomodasi extends Model
{
	public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function akomodasi()
     {
         return $this->belongsTo(Akomodasi::class,'akomodasi_id');
     }
}
