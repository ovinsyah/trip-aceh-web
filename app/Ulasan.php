<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
   public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function wisata()
     {
         return $this->belongsTo(Wisata::class,'wisata_id');
     }
}
