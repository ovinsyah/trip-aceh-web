<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengujian extends Model
{
    public function detail()
    {
        return $this->hasMany(PengujianDetail::class);
    }
}
