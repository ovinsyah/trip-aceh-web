<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akomodasi extends Model
{
    public function kota()
    {
        return $this->belongsTo(kota_aceh::class,'kota_aceh_id');
    }

     public function image()
    {
        return $this->hasMany(Image::class,'owner_id');
    }
    public function ulasan()
    {
        return $this->hasMany(UlasanAkomodasi::class,'akomodasi_id');
    }
    public function wisata()
    {
        return $this->belongsToMany(Wisata::class,'akomodasi_wisatas');
    }
}
