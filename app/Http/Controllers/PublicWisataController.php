<?php

namespace App\Http\Controllers;
use App\Ulasan;
use App\Wisata;
use App\kota_aceh;
use App\Akomodasi;
use App\Pengujian;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Http\Request;

class PublicWisataController extends Controller
{
    public function wisata()
    {
        $wisatas = Wisata::paginate(8);
        $wisataRekomen =(Auth::check())? Wisata::leftJoin('rekomendasis','wisatas.id', 'rekomendasis.wisata_id')
        ->where('rekomendasis.user_id', Auth::user()->id)
        ->select('wisatas.*',DB::raw('AVG(rekomendasis.rating) as rating'))
        ->groupBy('wisatas.id')
        ->orderBy('rating','DESC')
        ->take(4)->get():[];
    	$kotas = kota_aceh::All();
        $id=0;
        $kategori=0;
        return view('public.wisata',compact('wisatas','kotas','wisataRekomen','id','kategori'));
    }
    public function recommendation()
    {
        // $wisatas = DB::select(`SELECT * , SUM(u.rating) AS 'rating' from wisatas w LEFT JOIN ulasans u on w.id = u.wisata_id GROUP by w.id ORDER BY 'rating' DESC`);
        $wisatas = Wisata::leftJoin('rekomendasis','wisatas.id', 'rekomendasis.wisata_id')
        ->where('rekomendasis.user_id', Auth::user()->id)
        ->select('wisatas.*',DB::raw('AVG(rekomendasis.rating) as rating'))
        ->groupBy('wisatas.id')
        ->orderBy('rating','DESC')
        ->paginate(12);
        $kotas = kota_aceh::All();
        $id=0;
        $kategori=0;
    	return view('public.recommendation_wisata',compact('wisatas','kotas','id','kategori'));
    }
    public function toprate()
    {
        $wisatas = Wisata::leftJoin('ulasans','wisatas.id', 'ulasans.wisata_id')
        ->select(
            'wisatas.*',
            DB::raw('AVG(ulasans.rating) as rating')
        )
        ->groupBy('wisatas.id')
        ->orderBy('rating','DESC')
        ->paginate(12);
        $kotas = kota_aceh::All();
        $id=0;
        $kategori=0;
        return view('public.toprate_wisata',compact('wisatas','kotas','id','kategori'));
    }
    public function wisatadetail($id)
    {
        $wisata = Wisata::find($id);
        $akomodasis = Akomodasi::All();
        $ulasans=Ulasan::where('wisata_id',$id)->where('rating','!=',0)->get();
        //return dd($ulasans,$id);
    	return view('public.detail_wisata',compact('wisata','ulasans','akomodasis'));
    }

    public function filter_wisata($id,$kategori)
    {
        //return dd($id,$kategori);
        $wisatas=($id!=0)?(($kategori==0)?Wisata::where('kota_Aceh_id',$id)->paginate(12):Wisata::where('kota_Aceh_id',$id)->where('kategori',$kategori)->paginate(12)):(($kategori!=0)?Wisata::where('kategori',$kategori)->paginate(12):Wisata::paginate(12));
         $wisataRekomen =Wisata::leftJoin('ulasans','wisatas.id', 'ulasans.wisata_id')
        ->select(
            'wisatas.*',
            DB::raw('AVG(ulasans.rating) as rating')
        )
        ->groupBy('wisatas.id')
        ->orderBy('rating','DESC')
        ->take(4)->get();
         $kotas = kota_aceh::All();
         return view('public.wisata',compact('wisatas','kotas','id','wisataRekomen','kategori'));
    }
}
