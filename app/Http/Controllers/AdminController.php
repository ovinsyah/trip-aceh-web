<?php

namespace App\Http\Controllers;
use App\User;
use App\Ulasan;
use App\Rekomendasi;
use App\Wisata;
use App\Budaya;
use App\Akomodasi;
use App\Slider;
use App\Pengujian;
use App\PengujianDetail;
use File;
use Rumus;
use Illuminate\Http\Request;
ini_set('memory_limit', '3000M');
ini_set('max_execution_time', '0'); 
class AdminController extends Controller
{
    public function index()
    {
        $slider = Slider::All();
        $akomodasi = Akomodasi::All();
        $budaya = Budaya::All();
        $user = User::All();
        $wisata = Wisata::All();
        $ulasans = Ulasan::take(5)->where('rating','!=',0)->orderBy('created_at','Desc')->get();
        $sumulasan = Ulasan::where('rating','!=',0)->get();
        return view ('admin.index',compact('ulasans','user','wisata','budaya','akomodasi','slider','sumulasan'));
    }
    public function user()
    {
        $users = User::All();
        return view ('admin.user',compact('users'));
    }
    public function ulasan()
    {
        $ulasans = Ulasan::All();
    	return view ('admin.ulasan',compact('ulasans'));
    }
     public function delete_ulasan($id)
    {
        $ulasan = Ulasan::find($id);
        $ulasan->rating=0;
        $ulasan->save();

        $this->uji_isi(2,0.25,0.01,2);
        return back();
    }

    public function delete_user($id)
    {
       $user=User::find($id);
       if ($user->image) {
         $this->delete_foto($user->image);
       }
        $ulasan=Ulasan::where('user_id',$user->id)->delete();
       $user->delete();
        $this->uji_isi(2,0.25,0.01,2);

       return back();
    }

    public function delete_foto($img)
    {
       $image_path = public_path().'/images/User/'.$img;
       return dd($image_path);
          if(File::exists($image_path)){ 
              unlink($image_path);
        }
    }
    public function pengujian()
    {
        return view ('admin.pengujian');
    }
    public function hasilpengujian()
    {   
        $pengujians = Pengujian::all();
        return view ('admin.hasil_pengujian',compact('pengujians'));
    }
    public function detailhasilpengujian($id)
    {
        $pengujian = Pengujian::find($id);
        return view ('admin.detail_hasil_pengujian',compact('pengujian'));
    }
    public function uji(Request $request)
    {
        $tmax=$request->tmax;
        $k=$request->k;
        $w0=$request->w0;
        $lambda=$request->lambda;
        $ini=$this->uji_isi($tmax,$w0,$lambda,$k);

        return response()->json(['hasil'=>$ini['hasil'],'pengujian'=>$ini['pengujian']]);
    }

    public function uji_isi($tmax,$w0,$lambda,$k)
    {
       $hasil=Rumus::test($tmax,$w0,$lambda,$k);
       $pengujian=new Pengujian();
       $pengujian->mae=($hasil['mae']>0)?$hasil['mae']:$hasil['mae']*-1;
       $pengujian->tmax=$tmax;
       $pengujian->k=$k;
       $pengujian->w0=$w0;
       $pengujian->lambda=$lambda;
       if($pengujian->save()){
           foreach ($hasil['mae_nya'] as $val) {
               $rating=explode('|', $val)[0];
               $hasil2=explode('|', $val)[1];

               $dtl=new PengujianDetail();
               $dtl->pengujian_id=$pengujian->id;
               $dtl->rating=$rating;
               $dtl->hasil=$hasil2;
               $dtl->save();
           }
       }
       return ['hasil'=>$hasil,'pengujian'=>$pengujian];
    }
}
