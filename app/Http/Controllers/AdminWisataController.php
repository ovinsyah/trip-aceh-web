<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kota_aceh;
use App\Wisata;
use App\Image;
use App\Ulasan;
use App\User;
use File;

use App\Pengujian;
use App\PengujianDetail;
use Rumus;
class AdminWisataController extends Controller
{
    public function wisata()
    {
        $wisatas=Wisata::all();
    	return view ('admin.wisata',compact('wisatas'));
    }
    public function addwisata()
    {
        $kotas = kota_aceh::All();
        return view ('admin.add_wisata',compact('kotas'));
    }
    public function editwisata($id)
    {
        $kotas = kota_aceh::All();
        $wisata = Wisata::find($id);
    	return view ('admin.edit_wisata',compact('kotas','wisata'));
    }

    public function create_wisata(Request $request)
    {
        $wisata= new Wisata();
        $wisata->alamat_maps=$request->alamat_maps;
        $wisata->deskripsi=$request->deskripsi;
        $wisata->judul=$request->judul;
        $wisata->kategori=$request->kategori;
        $wisata->kota_aceh_id=$request->kota;
        $wisata->latlng_maps=$request->latlng_maps;
         if ($wisata->save()) {
            for ($i=0; $i <count($request->gambar) ; $i++) { 
                $image=new Image();   
                $image->name=$this->save_foto($request->gambar[$i],uniqid());
                $image->jenis="Wisata";
                $image->owner_id=$wisata->id;
                $image->save();  
            }
            $users=User::all();
            foreach ($users as $user) {
                $ulasan=new Ulasan();
                $ulasan->user_id=$user->id;
                $ulasan->wisata_id=$wisata->id;
                $ulasan->rating=0;
                $ulasan->save();
            }
             $this->uji();
            return "success";
         }

       return "Error";
    }



    public function update_wisata($id,Request $request)
    {
         $wisata= Wisata::find($id);
         $wisata->alamat_maps=$request->alamat_maps;
         $wisata->deskripsi=$request->deskripsi;
         $wisata->judul=$request->judul;
         $wisata->kategori=$request->kategori;
         $wisata->kota_aceh_id=$request->kota;
         $wisata->latlng_maps=$request->latlng_maps;
          if ($wisata->save()) {
             for ($j=0; $j <count($request->gambar_delete) ; $j++) {
                $hapus=Image::where('name',$request->gambar_delete[$j])->first();
                $this->delete_foto($hapus);
            }
            for ($i=0; $i <count($request->gambar) ; $i++) {
                $tmp=explode('/images/wisata/', $request->gambar[$i]);
                 if (count($tmp)<2) {
                     $image=new Image();   
                     $image->name=$this->save_foto($request->gambar[$i],uniqid());
                     $image->jenis="Wisata";
                     $image->owner_id=$wisata->id;
                     $image->save();  
                  }
             }
             return "success";
          }

        return "Error";
    }
    public function delete_wisata($id)
    {
       $wisata=Wisata::find($id);
       for ($i=0; $i <count($wisata->image) ; $i++) { 
            $this->delete_foto($wisata->image[$i]);
        }   
        $ulasan=Ulasan::where('wisata_id',$wisata->id)->delete();
       $wisata->delete();
       $this->uji();

       return back();
    }


  public function save_foto($img,$uniqid)
  {
    if (count(explode('.', $img))!=2) {
        $ext=explode('/', explode(';',explode(',', $img)[0])[0])[1];
        $img = explode(',', $img)[1];
        $data = base64_decode($img);
        $file =  public_path().'/images/Wisata/'.$uniqid.'.'.$ext;
        $success = file_put_contents($file, $data);
        return $uniqid.'.'.$ext;
    }else{
        return 'default.jpg';
    }
  }

  public function delete_foto($img)
  {
    if ($img->jenis=="Wisata") {
        $image_path = public_path().'/images/Wisata/'.$img->name;
        if ($img->name!="default.jpg") {
            if(File::exists($image_path)){ 
                unlink($image_path);
                $img->delete();
            }
        }
    }
  }

  public function uji()
  {
       $tmax=2;
        $k=2;
        $w0=0.25;
        $lambda=0.01;
        $hasil=Rumus::test($tmax,$w0,$lambda,$k);
        $pengujian=new Pengujian();
        $pengujian->mae=($hasil['mae']>0)?$hasil['mae']:$hasil['mae']*-1;
        $pengujian->tmax=$tmax;
        $pengujian->k=$k;
        $pengujian->w0=$w0;
        $pengujian->lambda=$lambda;
        if($pengujian->save()){
            foreach ($hasil['mae_nya'] as $val) {
                $rating=explode('|', $val)[0];
                $hasil2=explode('|', $val)[1];

                $dtl=new PengujianDetail();
                $dtl->pengujian_id=$pengujian->id;
                $dtl->rating=$rating;
                $dtl->hasil=$hasil2;
                $dtl->save();
            }
        }
  }




}
