<?php

namespace App\Http\Controllers;
use App\kota_aceh;
use App\Budaya;
use App\Image;
use File;
use Illuminate\Http\Request;

class AdminBudayaController extends Controller
{
    public function budaya()
    {
        $budayas=Budaya::all();
    	return view ('admin.budaya',compact('budayas'));
    }
    public function addbudaya()
    {
    	$kotas = kota_aceh::All();
    	return view ('admin.add_budaya',compact('kotas'));
    }
    public function editbudaya($id)
    {
    	$kotas = kota_aceh::All();
        $budaya=Budaya::find($id);
    	return view ('admin.edit_budaya',compact('kotas','budaya'));
    }

    public function create_budaya(Request $request)
    {
        $budaya=new Budaya();
        $budaya->deskripsi=$request->deskripsi;
        $budaya->judul=$request->judul;
        $budaya->kota_aceh_id=$request->kota;
        if($budaya->save()){
             for ($i=0; $i <count($request->gambar) ; $i++) { 
                $image=new Image();   
                $image->name=$this->save_foto($request->gambar[$i],uniqid());
                $image->jenis="Budaya";
                $image->owner_id=$budaya->id;
                $image->save();  
            }
            return "success";
        }
            return "Failed";
    }

    public function update_budaya($id,Request $request)
    {
        $budaya=Budaya::find($id);
        $budaya->deskripsi=$request->deskripsi;
        $budaya->judul=$request->judul;
        $budaya->kota_aceh_id=$request->kota;
        if($budaya->save()){
            if (isset($request->gambar_delete)) {
             for ($j=0; $j <count($request->gambar_delete) ; $j++) {
                   $hapus=Image::where('name',$request->gambar_delete[$j])->first();
                   $this->delete_foto($hapus);
               }
            }
            for ($i=0; $i <count($request->gambar) ; $i++) {
               $tmp=explode('/images/budaya/', $request->gambar[$i]);
                if (count($tmp)<2) {
                    $image=new Image();   
                    $image->name=$this->save_foto($request->gambar[$i],uniqid());
                    $image->jenis="Budaya";
                    $image->owner_id=$budaya->id;
                    $image->save();  
                 }
            }
            return "success";
        }
            return "Failed";
    }

     public function delete_budaya($id)
    {
       $budaya=Budaya::find($id);
       for ($i=0; $i <count($budaya->image) ; $i++) { 
            $this->delete_foto($budaya->image[$i]);
        }   
       $budaya->delete();

       return back();
    }

  public function save_foto($img,$uniqid)
  {
    if (count(explode('.', $img))!=2) {
        $ext=explode('/', explode(';',explode(',', $img)[0])[0])[1];
        $img = explode(',', $img)[1];
        $data = base64_decode($img);
        $file =  public_path().'/images/Budaya/'.$uniqid.'.'.$ext;
        $success = file_put_contents($file, $data);
        return $uniqid.'.'.$ext;
    }else{
        return 'default.jpg';
    }
  }

  public function delete_foto($img)
  {
    if ($img->jenis=="Budaya") {
        $image_path = public_path().'/images/Budaya/'.$img->name;
        if ($img->name!="default.jpg") {
            if(File::exists($image_path)){ 
                unlink($image_path);
                $img->delete();
            }
        }
    }
  }
}
