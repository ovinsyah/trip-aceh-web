<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Ulasan;
use Auth;
use Hash;
use App\Pengujian;
use App\PengujianDetail;
use Rumus;

class UserController extends Controller
{
    public function index()
    {
    	return view ('user.profile');
    }
    public function edit()
    {
    	return view ('user.edit_profile');
    }
    public function password()
    {
    	return view ('user.change_password');
    }

    public function update_profile(Request $request)
    {
        $user=Auth::user();
        $user->name=$request->nama;
         if ($request->hasFile('image')) {
            $img=$request->file('image');
            $ext=$img->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $img->move("images/User",$filenames);
            $user->image=$filenames;
         }
         $user->save();

       return redirect('/user/profile');
    }
    public function update_password(Request $request)
    {
        //return dd($request->all());
        $user=Auth::user();
        if (Hash::check($request->old,$user->password)) {
            $user->password=bcrypt($request->new);
            $user->save();
            return redirect('/user/profile')->with(['success' => 'berhasil merubah password']);
        }else{
            return redirect('/user/change-password')->with(['error' => 'gagal merubah password']);
        }
    }

    public function update_ulasan($id,Request $request)
    {
        $ulasan=Ulasan::where('user_id',$request->user_id)->where('wisata_id',$id)->first();
        $ulasan->rating=$request->rate;
        $ulasan->komentar=$request->ulasan;
        $ulasan->save();
        $this->uji();
        
        return 'sukses';
    }

    public function uji()
    {
         $tmax=2;
          $k=2;
          $w0=0.25;
          $lambda=0.01;
          $hasil=Rumus::test($tmax,$w0,$lambda,$k);
          $pengujian=new Pengujian();
          $pengujian->mae=($hasil['mae']>0)?$hasil['mae']:$hasil['mae']*-1;
          $pengujian->tmax=$tmax;
          $pengujian->k=$k;
          $pengujian->w0=$w0;
          $pengujian->lambda=$lambda;
          if($pengujian->save()){
              foreach ($hasil['mae_nya'] as $val) {
                  $rating=explode('|', $val)[0];
                  $hasil2=explode('|', $val)[1];

                  $dtl=new PengujianDetail();
                  $dtl->pengujian_id=$pengujian->id;
                  $dtl->rating=$rating;
                  $dtl->hasil=$hasil2;
                  $dtl->save();
              }
          }
    }

}
