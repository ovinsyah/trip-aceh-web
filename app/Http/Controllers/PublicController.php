<?php

namespace App\Http\Controllers;
use App\Wisata;
use App\Akomodasi;
use App\UlasanAkomodasi;
use App\Budaya;
use App\Slider;
use App\kota_aceh;
use DB;
use Backup;
use App\Pengujian;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PublicController extends Controller
{

    public function tester()
    {
        return view('public.backup');
    }
    public function backup()
    {
        $tmax=2;
        $k=2;
        $w0=0.25;
        $lambda=0.01;
        $hasil=Backup::test($tmax,$w0,$lambda,$k);
        $pengujian=new Pengujian();
        $pengujian->mae=($hasil['mae']>0)?$hasil['mae']:$hasil['mae']*-1;
        $pengujian->tmax=$tmax;
        $pengujian->k=$k;
        $pengujian->w0=$w0;
        $pengujian->lambda=$lambda;
        return response()->json(['hasil'=>$hasil,'pengujian'=>$pengujian]);
    }

    public function index()
    {   
        $sliders = Slider::All();
        $wisatas = Wisata::take(4)->get();
        $akomodasis = Akomodasi::take(4)->get();
        $budayas = Budaya::take(4)->get();
        $recomends=(Auth::check())?Wisata::leftJoin('rekomendasis','wisatas.id', 'rekomendasis.wisata_id')
        ->where('rekomendasis.user_id', Auth::user()->id)
        ->select('wisatas.*',DB::raw('AVG(rekomendasis.rating) as rating'))
        ->groupBy('wisatas.id')
        ->orderBy('rating','DESC')
        ->take(4)->get():[];
         $toprates= Wisata::leftJoin('ulasans','wisatas.id', 'ulasans.wisata_id')
        ->select('wisatas.*',DB::raw('AVG(ulasans.rating) as rating'))
        ->groupBy('wisatas.id')
        ->orderBy('rating','DESC')
        ->take(4)->get();
    	return view('public.index',compact('wisatas','akomodasis','budayas','sliders','recomends','toprates'));
    }
   
    public function akomodasi()
    {
        $kotas = kota_aceh::All();
        $akomodasis = Akomodasi::paginate(16);
        $key='Semua Lokasi';
        return view('public.akomodasi',compact('akomodasis','kotas','key'));
    }
    public function akomodasidetail($id)
    {
        $akomodasi = Akomodasi::find($id);
        $ulasans=UlasanAkomodasi::where('akomodasi_id',$id)->get();
        return view('public.detail_akomodasi',compact('akomodasi','ulasans'));
    }
    public function budaya()
    {
        $kotas = kota_aceh::All();
        $budayas = Budaya::paginate(16);
        return view('public.budaya',compact('budayas','kotas'));
    }
    public function budayadetail($id)
    {
        $budaya = Budaya::find($id);
    	return view('public.detail_budaya',compact('budaya'));
    }
    public function search($key)
    {
        $wisatas=Wisata::where('judul','LIKE','%'.$key.'%')->get();
        //return dd($wisatas);
        return view('public.search_wisata',compact('wisatas','key'));
    }
    public function sliderdetail($id)
    {
        $slider = Slider::find($id);
        return view('public.detail_slider',compact('slider'));
    }

    public function filter_akomodasi($id)
    {
        $akomodasis=($id!=0)?Akomodasi::where('kota_Aceh_id',$id)->paginate(16):Akomodasi::paginate(16);
         $kotas = kota_aceh::All();
         $key=($id!=0)?kota_aceh::find($id)->name:'Semua Lokasi';
         //return dd($akomodasis);
         return view('public.akomodasi',compact('akomodasis','kotas','key'));
    }

}
