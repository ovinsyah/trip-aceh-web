<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Image;
use File;
class AdminSliderController extends Controller
{
    public function slider()
    {
        $sliders=Slider::all();
    	return view ('admin.slider',compact('sliders'));
    }
    public function addslider()
    {
    	return view ('admin.add_slider');
    }
    public function editslider($id)
    {
        $slider=Slider::find($id);
    	return view ('admin.edit_slider',compact('slider'));
    }

    public function create_slider(Request $request)
    {
        $slider=new Slider();
        $slider->deskripsi=$request->deskripsi;
        $slider->judul=$request->judul;
        $slider->image=$this->save_foto($request->gambar[0],uniqid());
        if($slider->save()){
            return "Success";
        }
        return "Failed";
    }

    public function update_slider($id,Request $request)
    {
        $slider=Slider::find($id);
        $slider->deskripsi=$request->deskripsi;
        $slider->judul=$request->judul;
        if (isset($request->gambar)) {
            $image_path = public_path().'/images/Slider/'.$slider->image;
             if(File::exists($image_path)){unlink($image_path);}

          $slider->image=$this->save_foto($request->gambar[0],uniqid());
        }
        if($slider->save()){
            return "Success";
        }
        return "Failed";
    }

     public function delete_slider($id)
    {
       $slider=Slider::find($id);
         $image_path = public_path().'/images/Slider/'.$slider->image;
            if(File::exists($image_path)){ 
                  unlink($image_path);
                  $slider->delete();
              }
       return back();
    }

    public function save_foto($img,$uniqid)
    {
      if (count(explode('.', $img))!=2) {
          $ext=explode('/', explode(';',explode(',', $img)[0])[0])[1];
          $img = explode(',', $img)[1];
          $data = base64_decode($img);
          $file =  public_path().'/images/Slider/'.$uniqid.'.'.$ext;
          $success = file_put_contents($file, $data);
          return $uniqid.'.'.$ext;
      }else{
          return 'default.jpg';
      }
    }

}
