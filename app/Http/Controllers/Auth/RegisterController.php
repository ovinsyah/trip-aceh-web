<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Ulasan;
use App\Wisata;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Pengujian;
use App\PengujianDetail;
use Rumus;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
       /* return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);*/
        $user=new User();
        $user->name=$data['name'];
        $user->email=$data['email'];
        $user->password=bcrypt($data['password']);
        if($user->save()){
            foreach (Wisata::all() as $wisata) {
                $ulasan=new Ulasan();
                $ulasan->user_id=$user->id;
                $ulasan->wisata_id=$wisata->id;
                $ulasan->rating=0;
                $ulasan->save();
            }
        }
        $this->uji();
        return $user;

    }

    public function uji()
    {
        $tmax=2;
        $k=2;
        $w0=0.25;
        $lambda=0.01;
        $hasil=Rumus::test($tmax,$w0,$lambda,$k);
          $pengujian=new Pengujian();
          $pengujian->mae=($hasil['mae']>0)?$hasil['mae']:$hasil['mae']*-1;
          $pengujian->tmax=$tmax;
          $pengujian->k=$k;
          $pengujian->w0=$w0;
          $pengujian->lambda=$lambda;
          if($pengujian->save()){
              foreach ($hasil['mae_nya'] as $val) {
                  $rating=explode('|', $val)[0];
                  $hasil2=explode('|', $val)[1];

                  $dtl=new PengujianDetail();
                  $dtl->pengujian_id=$pengujian->id;
                  $dtl->rating=$rating;
                  $dtl->hasil=$hasil2;
                  $dtl->save();
              }
          }
    }
}
