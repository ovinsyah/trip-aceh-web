<?php

namespace App\Http\Controllers;
use App\kota_aceh;
use App\Wisata;
use App\Akomodasi;
use App\UlasanAkomodasi;
use App\AkomodasiWisata;
use App\Image;
use File;
use DB;
use Illuminate\Http\Request;

class AdminAkomodasiController extends Controller
{
    public function akomodasi()
    {
        $akomodasis=Akomodasi::all();
    	return view ('admin.akomodasi',compact('akomodasis'));
    }
    public function addakomodasi()
    {	
      $kotas = kota_aceh::All();
      $wisatas = Wisata::All();
      return view ('admin.add_akomodasi',compact('kotas','wisatas'));
    }
    public function editakomodasi($id)
    { 
      $kotas = kota_aceh::All();
      $akomodasi = Akomodasi::find($id);
    	$wisatas = Wisata::All();
    	return view ('admin.edit_akomodasi',compact('kotas','akomodasi','wisatas'));
    }

    public function create_akomodasi(Request $request)
    {
     // return $request;
        $akomodasi=new Akomodasi();
        $akomodasi->deskripsi=$request->deskripsi;
        $akomodasi->judul=$request->judul;
        $akomodasi->kota_aceh_id=$request->kota;
       if ($akomodasi->save()) {
           $wisatas=$request->wisata_terdekat;
              for ($x=0; $x <count($wisatas) ; $x++) { 
                   $relasi= new AkomodasiWisata();
                   $relasi->wisata_id=$wisatas[$x];
                   $relasi->akomodasi_id=$akomodasi->id;
                   $relasi->save();
              }
            for ($i=0; $i <count($request->gambar) ; $i++) { 
                $image=new Image();   
                $image->name=$this->save_foto($request->gambar[$i],uniqid());
                $image->jenis="Akomodasi";
                $image->owner_id=$akomodasi->id;
                $image->save(); 
            }
            return $akomodasi;
        } 
        return "Error";
    }

    public function update_akomodasi($id,Request $request)
    {
      //return count($request->gambar_delete);
        $akomodasi=Akomodasi::find($id);
        $akomodasi->deskripsi=$request->deskripsi;
        $akomodasi->judul=$request->judul;
        $akomodasi->kota_aceh_id=$request->kota;
       if ($akomodasi->save()) {
        $delete = AkomodasiWisata::where('akomodasi_id', $id)->delete();
        $wisatas=$request->wisata_terdekat;
        if ($wisatas) {
          for ($x=0; $x <count($wisatas) ; $x++) { 
                     $relasi= new AkomodasiWisata();
                     $relasi->wisata_id=$wisatas[$x];
                     $relasi->akomodasi_id=$akomodasi->id;
                     $relasi->save();
                }
        }
        // DB::delete('delete akomodasi_wisatas where akomodasi_id = ?', [$id]);
          if ($request->gambar_delete) {
               for ($j=0; $j <count($request->gambar_delete) ; $j++) {
                      $hapus=Image::where('name',$request->gambar_delete[$j])->first();
                      $this->delete_foto($hapus);
                  }
          }
         for ($i=0; $i <count($request->gambar) ; $i++) {
            $tmp=explode('/images/akomodasi/', $request->gambar[$i]);
             if (count($tmp)<2) {
                 $image=new Image();   
                 $image->name=$this->save_foto($request->gambar[$i],uniqid());
                 $image->jenis="Akomodasi";
                 $image->owner_id=$akomodasi->id;
                 $image->save();  
              }
         }
            return "success";
        } 
        return "Error";
    }

    public function delete_akomodasi($id)
    {
       $akomodasi=Akomodasi::find($id);
       for ($i=0; $i <count($akomodasi->image) ; $i++) { 
            $this->delete_foto($akomodasi->image[$i]);
        }   
       $akomodasi->delete();

       return back();
    }


    public function create_ulasan_akomodasi(Request $request)
    {
      $ulasan=new UlasanAkomodasi();
      $ulasan->akomodasi_id=$request->akomodasi_id;
      $ulasan->user_id=$request->user_id;
      $ulasan->rating=$request->rate;
      $ulasan->komentar=$request->ulasan;
      $ulasan->save();
      
      return $ulasan;
    }

    public function save_foto($img,$uniqid)
    {
      if (count(explode('.', $img))!=2) {
          $ext=explode('/', explode(';',explode(',', $img)[0])[0])[1];
          $img = explode(',', $img)[1];
          $data = base64_decode($img);
          $file =  public_path().'/images/Akomodasi/'.$uniqid.'.'.$ext;
          $success = file_put_contents($file, $data);
          return $uniqid.'.'.$ext;
      }else{
          return 'default.jpg';
      }
    }

    public function delete_foto($img)
    {
      if ($img->jenis=="Akomodasi") {
          $image_path = public_path().'/images/Akomodasi/'.$img->name;
          if ($img->name!="default.jpg") {
              if(File::exists($image_path)){ 
                  unlink($image_path);
                  $img->delete();
              }
          }
      }
    }


}
