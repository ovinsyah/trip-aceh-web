<?php

namespace App\Http\Helper;
use Illuminate\Contracts\View\View;
use App\Tester;
use App\tblP;
use App\tblQ;
use App\User;
use App\Ulasan;
use App\Wisata;
use App\Rekomendasi;
use NumPHP\Core\NumArray;
use NumPHP\LinAlg\LinAlg;
ini_set('memory_limit', '3000M');
ini_set('max_execution_time', '0'); 
class Rumus
{

	function __construct()
	{
	}

	public static function Test($iterasi,$w0,$lambda,$k)
	{
		$history=array();
		for ($it=0; $it <$iterasi ; $it++) { 
					
				$GLOBALS['k']=$k;
				$k_laten=$GLOBALS['k'];

				/*
				$users=[1,2,3,4]; //list id user untuk mengetahui user yg telah memberikan rating
				$wisatas=[1,2,3,4];//list id wisata untuk mengetahui wisata yg telah diberikan rating
				$mat_P= [[0.1,0.2],[-0.3,-0.4],[0.2,0.1],[-0.6,0.5]];//nilai random -1 1 matrix P
				$mat_Q= [[0.3,-0.1],[0.7,0.4],[-0.7,-0.8],[0.1,0.5]];//nilai random -1 1 matrix Q
				$matrix_rating=[[5 ,3, 0 ,3],[0,3,5,2],[0 ,5 ,2 ,2],[4 ,3, 0 ,4]];

*/
				$users=User::all()->pluck('id'); //list id user untuk mengetahui user yg telah memberikan rating
				$wisatas=Wisata::all()->pluck('id');//list id wisata untuk mengetahui wisata yg telah diberikan rating
			/*	$mat_P=Rumus::random(count($users),$k_laten);
				$mat_Q=Rumus::random(count($wisatas),$k_laten);*/
				$matrix_rating=Rumus::createM($users,$wisatas);
				$mat_P=($it==0)?Rumus::random(count($users),$k_laten,'P',$k):Rumus::normaliasiM($GLOBALS['P']);
				$mat_Q=($it==0)?Rumus::random(count($wisatas),$k_laten,'Q',$k):Rumus::normaliasiM($GLOBALS['Q']);
				//if($it>0){return ['mat_Q'=>$mat_Q,'mat_P'=>$mat_P,"P"=>Rumus::normaliasiM($GLOBALS['P']),"Q"=>Rumus::normaliasiM($GLOBALS['Q'])];}
				$Ptrans=Rumus::transpose($mat_P);//hasil transpose matrix p
				$Qtrans=Rumus::transpose($mat_Q);//hasil transpose matrix q
				$Sp=Rumus::multiple_M($Ptrans,$mat_P);//hasil perkalian matrix p x ptrans
				$Sq=Rumus::multiple_M($Qtrans,$mat_Q);//hasil perkalian matrix q x qtrans
				$GLOBALS['P']=array();// inisasi nilai awal varibel P perubahan
				$GLOBALS['Q']=array();// inisasi nilai awal varibel Q perubahan
				$arr_ap=array();
				$arr_ap_awal=array();
				$arr_ap_inv=array();
				$arr_p_vector=array();
				$arr_aq=array();
				$arr_aq_awal=array();
				$arr_aq_inv=array();
				$arr_q_vector=array();
					//$x=Rumus::Kn_P($Sp,$Qtrans,1,1,$w0,$users[0]);//fungsi untuk mencari nilai berdsarkan k1 dan k2 pada user yang ke N

				//return ['mat_Q'=>$mat_Q,'mat_P'=>$mat_P,'Ptrans'=>$Ptrans,'Qtrans'=>$Qtrans,'Sp'=>$Sp,'Sq'=>$Sq,'P'=>$GLOBALS['Ap'],"x"=>$x];
				for ($i=0; $i <count($users) ; $i++) { //looping untuk mencari nilai P user 1 -n
					// Rumus::Kn_P($Sp,$Qtrans,1,1,$w0,$users[$i]);
					// Rumus::Kn_P($Sp,$Qtrans,1,2,$w0,$users[$i]);
					// Rumus::Kn_P($Sp,$Qtrans,2,1,$w0,$users[$i]);
					// Rumus::Kn_P($Sp,$Qtrans,2,2,$w0,$users[$i]);
					Rumus::loop_K($Sp,$Qtrans,$w0,$users[$i],'P');

					//step 6
				
					Rumus::loop_AP($lambda,'P');
					$arr_ap[]=$GLOBALS['Ap'];
					$arr_ap_awal[]=$GLOBALS['Ap_awal'];
					$Ap_inv=LinAlg::inv($GLOBALS['Ap']);//disini masalah nya
					$arr_ap_inv[]=$Ap_inv->data;
					$arr_p_vector[]=Rumus::vector_p();
					$GLOBALS['P'][]=Rumus::multiple_M($Ap_inv->data,Rumus::vector_p());
					//end step 6
				}
				for ($i=0; $i <count($wisatas) ; $i++) { //looping untuk mencari nilai Q wisata 1 -n
					/*Rumus::Kn_Q($GLOBALS['P'],$Sq,1,1,$w0,$wisatas[$i]);
					Rumus::Kn_Q($GLOBALS['P'],$Sq,1,2,$w0,$wisatas[$i]);
					Rumus::Kn_Q($GLOBALS['P'],$Sq,2,1,$w0,$wisatas[$i]);
					Rumus::Kn_Q($GLOBALS['P'],$Sq,2,2,$w0,$wisatas[$i]);*/
					Rumus::loop_K($GLOBALS['P'],$Sq,$w0,$wisatas[$i],'Q');
					//start step 6
					Rumus::loop_AP($lambda,'Q');
					$arr_aq[]=$GLOBALS['Aq'];
					$arr_aq_awal[]=$GLOBALS['Aq_awal'];
					$Aq_inv=LinAlg::inv($GLOBALS['Aq']);
					$arr_aq_inv[]=$Aq_inv->data;
					$arr_q_vector[]=Rumus::vector_q();
					$GLOBALS['Q'][]=Rumus::multiple_M($Aq_inv->data,Rumus::vector_q());
					//return dd(['Aq'=>$GLOBALS['Aq'],"AQin"=>$Aq_inv,'Q'=>$GLOBALS['Q'],"vp"=>Rumus::vector_q(),'q1'=>$GLOBALS['q1'],'q2'=>$GLOBALS['q2']]);
					//end step 6
				}
				//return ['mat_Q'=>$mat_Q,'mat_P'=>$mat_P,"P"=>$GLOBALS['P'],"Q"=>$GLOBALS['Q'],'Sp'=>$Sp,'Sq'=>$Sq,'Aq'=>$GLOBALS['Aq']];
			$history[]=(object)["iterasi".$it=>["q_vector"=>$arr_q_vector,"p_vector"=>$arr_p_vector,"Sp"=>$Sp,"Sq"=>$Sq,"P"=>$GLOBALS['P'],"Q"=>$GLOBALS['Q'],"mat_P"=>$mat_P,"mat_Q"=>$mat_Q,"Aq"=>$arr_aq,"Ap_awal"=>$arr_ap_awal,"Aq_awal"=>$arr_aq_awal,"Ap"=>$arr_ap,'Aq_inv'=>$arr_aq_inv,'Ap_inv'=>$arr_ap_inv]];
			}
		$pQtrans=Rumus::pQtrans();
		$Mae=Rumus::getMae($pQtrans,$matrix_rating);
			//return ["history"=>$history,"qPtrans"=>Rumus::pQtrans(),"rating"=>$matrix_rating,"MAE"=>$Mae];
		 return ["mae"=>$Mae,"topN"=>$GLOBALS['topN'],"history"=>$history,'mat_Q'=>$mat_Q,'mat_P'=>$mat_P,'mae_nya'=>$GLOBALS['mae_nya'],"pQtrans"=>$pQtrans,"users"=>$users,"wisatas"=>$wisatas,"rating"=>$matrix_rating];

	}

	public static function loop_AP($lambda,$pq)
	{
		for ($i=0; $i <$GLOBALS['k'] ; $i++) { 
			if ($pq=='P') {
				$GLOBALS['Ap'][$i][$i]+=$lambda;
			}else{
				$GLOBALS['Aq'][$i][$i]+=$lambda;
			}
		}
	}

	public static function loop_K($a,$b,$e,$f,$g)
	{
		for ($i=1; $i <=$GLOBALS['k'] ; $i++) { 
			for ($j=1; $j <=$GLOBALS['k'] ; $j++) { 
						if ($g=='Q') {
							Rumus::Kn_Q($a,$b,$i,$j,$e,$f);
						}else{
							Rumus::Kn_P($a,$b,$i,$j,$e,$f);
						}
					}		
		}
	}

	public static function normaliasiM($m)
	{
		$matrix;
		for ($i=0; $i <count($m) ; $i++) { 
			for ($j=0; $j <count($m[0]) ; $j++) { 
						$matrix[$i][$j]=$m[$i][$j][0];
					}		
		}
		return $matrix;
	}
	public static function random($x,$y,$tbl,$k)//untuk mencari nilai random -1 1
	{
		$matrix;
		//return dd($x,$y);
		$a=array(-1,1);
		$cek=($tbl=='P')?tblP::where('k',$k)->get():tblQ::where('k',$k)->get();
		//return dd(count($cek),$x,$y);
		if (count($cek)==0 || count($cek)!=($x*2) ) {
			for ($i=0; $i <$x ; $i++) { 
				for ($j=0; $j <$y ; $j++) { 
					$h=array(1,-1,($a[array_rand($a,1)]*round(rand()/getrandmax(),2)));
					$nilai=$h[array_rand($h,1)];
					$matrix[$i][$j]=$nilai;
					$random=($tbl=='P')?new tblP():new tblQ();
					$random->k=$k;
					$random->row=$i;
					$random->column=$j;
					$random->nilai=$nilai;
					$random->save();
				}		
			}
			
		}else{
			for ($i=0; $i <$x ; $i++) { 
				for ($j=0; $j <$y ; $j++) { 
					$matrix[$i][$j]=($tbl=='P')?tblP::where([['k',$k],['row',$i],['column',$j]])->first()->nilai:tblQ::where([['k',$k],['row',$i],['column',$j]])->first()->nilai;
				}
			}
		}
		return $matrix;
	}
	public static function satu($x,$y)//untuk mencari nilai random -1 1
	{
		$matrix;
		for ($i=0; $i <$x ; $i++) { 
			for ($j=0; $j <$y ; $j++) { 
						$matrix[$i][$j]=1;
					}		
		}
		return $matrix;
	}


	public static function transpose($trans)
	{
		array_unshift($trans, null);
		$trans = call_user_func_array('array_map', $trans);
		return $trans;
	}


	public static function multiple_M($a,$b)//perkalian matrix
	{
		$r=count($a);
		$c=count($b[0]);
		$p=count($b);
		if(count($a[0]) != $p){
		    return "Incompatible matrices";
		}
		$result=array();
		for ($i=0; $i < $r; $i++){
		    for($j=0; $j < $c; $j++){
		        $result[$i][$j] = 0;
		        for($k=0; $k < $p; $k++){
		            $result[$i][$j] += $a[$i][$k] * $b[$k][$j];
		        }
		    }
		}
		return $result;
	}
		public static function multiple_M2($a,$b)//perkalian matrix
	{
		$r=count($a);
		$c=count($b[0]);
		$p=count($b);
		if(count($a[0]) != $p){
		    return "Incompatible matrices";
		}
		$result=array();
		for ($i=0; $i < $r; $i++){
		    for($j=0; $j < $c; $j++){
		        $result[$i][$j] = 0;
		        for($k=0; $k < $p; $k++){
		            $result[$i][$j] += $a[$i][$k][0] * $b[$k][$j][0];
		        }
		    }
		}
		return $result;
	}

	public static function Pbaru($matrix,$w0)//step 4
	{
		$result=array();
		for ($i=0; $i <count($matrix) ; $i++) { 
			for ($j=0; $j <count($matrix[$i]) ; $j++) { 
				$result[$i][$j]=$matrix[$i][$j]*$w0;
			}
		}
		return $result;
	}

	public static function Kn_P($Hq,$Sq,$k1,$k2,$w0,$user_id)//step5
	{

		$K_ap=array();
		$asli=Ulasan::where('user_id',$user_id)->get();
		$tmp_k1=$Sq[$k1-1];
		$tmp_k2=$Sq[$k2-1];
		$n=0;
		for ($i=1; $i <=$GLOBALS['k'] ; $i++) { 
			$GLOBALS['p'.$i]=0;
		}
		$hasil=0;
		for ($i=0; $i <count($tmp_k1); $i++) {
			if ($asli[$i]->rating!=0) {
				for ($j=0; $j <$GLOBALS['k'] ; $j++) { 
					$GLOBALS['p'.($j+1)]+=$Sq[$j][$i]*$asli[$i]->rating;
				}
				//$GLOBALS['p2']+=$Sq[1][$i]*$asli[$i]->rating;
				$K_ap[$n]=$tmp_k1[$i];
				$K_ap[$n+1]=$tmp_k2[$i];
				$hasil+=$K_ap[$n]*$K_ap[$n+1];
				$n+=2;
			 } 
		}
		$hasil*=(1-$w0);
		if ($k1==1 && $k2==1) {
			$GLOBALS['Ap']=Rumus::Pbaru($Hq,$w0);
			$GLOBALS['Ap_awal']=Rumus::Pbaru($Hq,$w0);
		}else{
			$GLOBALS['Ap'];
		}
		$GLOBALS['Ap'][$k1-1][$k2-1]+=$hasil;
		return $GLOBALS['Ap'];
	}


	public static function Kn_Q($P,$Sq,$k1,$k2,$w0,$wisata_id)//step 5
	{
		$K_aq=array();
		$asli=Ulasan::where('wisata_id',$wisata_id)->get();
		$n=0;
		for ($i=1; $i <=$GLOBALS['k'] ; $i++) { 
			$GLOBALS['q'.$i]=0;
		}
		$hasil=0;
		for ($i=0; $i <count($P); $i++) {
			if ($asli[$i]->rating!=0) {
				for ($j=0; $j <$GLOBALS['k'] ; $j++) { 
					$GLOBALS['q'.($j+1)]+=$P[$i][$j][0]*$asli[$i]->rating;
				}
				//$GLOBALS['q2']+=$P[$i][1][0]*$asli[$i]->rating;
				$K_aq[$n]=$P[$i][$k1-1];
				$K_aq[$n+1]=$P[$i][$k2-1];
				$hasil+=$K_aq[$n][0]*$K_aq[$n+1][0];
				$n+=2;
			 } 
		}
		if ($k1==1 && $k2==1) {
			$GLOBALS['Aq']=Rumus::Pbaru($Sq,$w0);
			$GLOBALS['Aq_awal']=Rumus::Pbaru($Sq,$w0);
		}else{
			$GLOBALS['Aq'];
		}
		$hasil*=(1-$w0);
		$GLOBALS['Aq'][$k1-1][$k2-1]+=$hasil;
		return $GLOBALS['Aq'];
	}

	public static function vector_p()
	{
		$vector=array();
		for ($i=1; $i <=$GLOBALS['k'] ; $i++) { 
			$vector[][0]=$GLOBALS['p'.$i];
		};

		return $vector;
	}

	public static function vector_q()
	{
		$vector=array();
		for ($i=1; $i <=$GLOBALS['k'] ; $i++) { 
			$vector[][0]=$GLOBALS['q'.$i];
		};

		return $vector;
	}

	public static function pQtrans()
	{
		$qtrans=Rumus::transpose($GLOBALS['Q']);
		$Pqtrans=Rumus::multiple_M2($GLOBALS['P'],$qtrans);

		return $Pqtrans;
	}

	public static function getMae($pqTrans,$rate)
	{

		$r=count($pqTrans);//12 bnyk user
		$c=count($pqTrans[0]);//79 //bnyk wisata

		$users=User::all()->pluck('id'); //list id user untuk mengetahui user yg telah memberikan rating
		$wisatas=Wisata::all()->pluck('id');//list id wisata untuk mengetahui wisata yg telah diberikan rating

		$mae=0;
		$per=0;
		Rekomendasi::truncate();
		for ($i=0; $i < $r; $i++){
		    for($j=0; $j < $c; $j++){
		        	if ($rate[$j][$i]!=0) {
		        		$per++;
		            //$mae.=$rate[$i][$j]."-".$pqTrans[$i][$j]."+";
		        		$GLOBALS['mae_nya'][]=$rate[$j][$i].'|'.$pqTrans[$i][$j];
		            $mae+=$rate[$j][$i]-$pqTrans[$i][$j];
		        	}else{
		            	$GLOBALS['topN'][$i][$j] = $pqTrans[$i][$j];
		        		$rekomendasi= new Rekomendasi();
		        		$rekomendasi->user_id=$users[$i];
		        		$rekomendasi->wisata_id=$wisatas[$j];
		        		$rekomendasi->rating=$pqTrans[$i][$j];
		        		$rekomendasi->save();
		        	}
		    }
		}
		return $mae/$per;
	}
	public static function createM($row,$column)
	{
		$r=count($row);
		$c=count($column);
		$result=array();
		for ($i=0; $i < $c; $i++){
		    for($j=0; $j < $r; $j++){
		        $result[$i][$j]=Ulasan::where('user_id',$row[$j])->where('wisata_id',$column[$i])->first()->rating;
		    }
		}
		return $result;
	}




}
