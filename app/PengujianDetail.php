<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengujianDetail extends Model
{
    public function wisata()
	{
     return $this->belongsTo('App\Wisata','wisata_id');
	}
}
