@extends('master.master_admin')
@section('content')
<div style="overflow-x: scroll; min-height: 81vh">
	<div class="title-admin mb-3">Pengujian</div>
	<div class="text-center font-18 text-bold mb-4 mt-5">
		Parameter Pengujian
	</div>
	<!-- <form method="post" action="{{url('/admin/uji')}}"> -->
		<div class="row m-0 mb-3">
			<div class="col text-right">
				<span class="text-bold mr-3">tmax</span>
				<input type="" name="tmax" class="form-control d-inline-block" style="width: 12rem" id="tmax">
			</div>
			<div class="col">
				<input type="" name="k" class="form-control d-inline-block"  style="width: 12rem" value="2" id="k">
				<span class="text-bold ml-3">K</span>
			</div>
		</div>
		<div class="row m-0 mb-3">
			<div class="col text-right">
				<span class="text-bold mr-3">W0</span>
				<input type="" name="w0" class="form-control d-inline-block" readonly=""  value="0.25" style="width: 12rem" id="w0">
			</div>
			<div class="col">
				<input type="" name="lambda" class="form-control d-inline-block" readonly=""  style="width: 12rem" value="0.01" id="lambda">
				<span class="text-bold ml-3">&lambda;</span>
			</div>
		</div>
		<div class="text-center mt-3">
			<div>
				<button class="btn btn-app" id="process" type="submit">Proses</button>
			</div>
		</div>
		<!-- Rendering -->
		<div class="hidden" id="view-algo">
			<div class="mt-5 text-bold">Tabel Rating</div>
			<table class="table table-striped">
				<tbody id="table-rating"></tbody>
			</table>
			<div class="panel-group mt-5 iterasi" id="accordion"></div>
			<div class="panel-group" id="accordion2">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapse-link" data-toggle="collapse" data-parent="#accordion2" href="#collapsen">Nilai PQ Trans</a>
						</h4>
					</div>
					<div id="collapsen" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table table-striped">
								<tbody id="pq_trans"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-group" id="accordion3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapse-link" data-toggle="collapse" data-parent="#accordion3" href="#collapsemae">Nilai MAE</a>
						</h4>
					</div>
					<div id="collapsemae" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row m-0">
								<div class="col text-justify pl-0">
									<div id="mae"></div>
								</div>
								<div class="col text-center pr-0" style="max-width: 23rem;padding-top: 50%;font-size: 38px;">
									<div id="length_mae"></div>
									= <span id="sum_mae"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- </form> -->
		@php($history =session('hasil'))
		@if($history)
		{{$history->history[0]}}
		@endif


		@if($pengujian=session('pengujian'))
		<div class="font-16">
			<span class="text-bold mr-2">MAE :</span><span>{{number_format( $pengujian->mae,3)}}</span>
		</div>
		<div class="content-admin">
			<table id="table_id" class="table table-striped">
				<thead>
					<tr>
						<th>rating</th>
						<th>Hasil</th>
					</tr>
				</thead>
				<tbody>
					@foreach($pengujian->detail as $detail)
					<tr>
						<td>{{$detail->rating}}</td>
						<td>{{$detail->hasil}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		@endif
	</div>
	<script type="text/javascript">
		window.onload = function() {
			$('#sidebar').addClass('active');
		}
		$('#table_id').DataTable({
			"order": [[ 1, "desc" ]],
			"iDisplayLength":20
		});
		$('#process').click(function () {
			$('#process').attr('disabled',true);
			$('#process').text('Sedang diproses');
			$('#view-algo').addClass('hidden');
			$('#table-rating').empty();
			$('#step').empty();
			$('#pq_trans').empty();
			$('#mae').empty();
			var data = {
				'tmax':$('#tmax').val(),
				'k':$('#k').val(),
				'w0':$('#w0').val(),
				'lambda':$('#lambda').val(),
			}

			$.ajax({
				url: "/api/uji",
				type: "POST",
				data:  data, 
				success:function(data){
					$('#process').attr('disabled',false);
					$('#process').text('Proses');
					$('#view-algo').removeClass('hidden');
					var table = data.hasil.rating;
					var history = data.hasil.history;
					var pq_trans = data.hasil.pQtrans;
					var mae = data.hasil.mae_nya;
					var sum_mae = (data.hasil.mae>0)?data.hasil.mae:data.hasil.mae*-1;
					console.log(data);
					//loop table rating
					for(a=0; a<table[0].length;a++){
						$('#table-rating').append(`
							<tr id="sub-table`+a+`"></tr>
						`)
						for(b=0;b<table.length;b++){
							$('#sub-table'+a).append(`
								<td>`+table[b][a]+`</td>
							`)
						}
					}
					//loop iterasi
					for(i=0;i<history.length;i++){
						//deklarasi variable
						var iterasi = 'iterasi'+i;
						var p = history[i][iterasi].P;
						var q = history[i][iterasi].Q;
						var sp = history[i][iterasi].Sp;
						var sq = history[i][iterasi].Sq;
						var mat_p = history[i][iterasi].mat_P;
						var mat_q = history[i][iterasi].mat_Q;
						var ap = history[i][iterasi].Ap_awal;
						var aq = history[i][iterasi].Aq_awal;
						var ap_new = history[i][iterasi].Ap;
						var aq_new = history[i][iterasi].Aq;
						var ap_inv = history[i][iterasi].Ap_inv;
						var aq_inv = history[i][iterasi].Aq_inv;
						var p_vector = history[i][iterasi].p_vector;
						var q_vector = history[i][iterasi].q_vector;
						$('.iterasi').append(`
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="collapse-link" data-toggle="collapse" data-parent="#accordion" href="#collapse`+(i+1)+`">
										Iterasi `+(i+1)+`</a>
									</h4>
								</div>
								<div id="collapse`+(i+1)+`" class="panel-collapse collapse">
									<div class="panel-body row m-0">
										<div class="col pl-0 text-center">
											<label>Nilai P</label>
											<table id="" class="table table-striped">
												<tbody id="mat_p`+i+`"></tbody>
											</table>
										</div>
										<div class="col text-center">
											<label>Nilai Q</label>
											<table id="" class="table table-striped">
												<tbody id="mat_q`+i+`"></tbody>
											</table>
										</div>
										<div class="col text-center">
											<div class="row m-0">
												<div class="col text-center">
													<label>Nilai SP</label>
													<table id="" class="table table-striped">
														<tbody id="sp`+i+`"></tbody>
													</table>
												</div>
												<div class="col pr-0 text-center">
													<label>Nilai SQ</label>
													<table id="" class="table table-striped">
														<tbody id="sq`+i+`"></tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-body row m-0">
										<div class="col pl-0 text-center">
											<label>Nilai AP</label>
											<table id="" class="table table-striped">
												<tbody id="ap`+i+`"></tbody>
											</table>
										</div>
										<div class="col text-center">
											<label>Nilai AQ</label>
											<table id="" class="table table-striped">
												<tbody id="aq`+i+`"></tbody>
											</table>
										</div>
										<div class="col pl-0 text-center">
											<label>Nilai AP Pembaharuan</label>
											<table id="" class="table table-striped">
												<tbody id="ap_new`+i+`"></tbody>
											</table>
										</div>
										<div class="col text-center">
											<label>Nilai AQ Pembaharuan</label>
											<table id="" class="table table-striped">
												<tbody id="aq_new`+i+`"></tbody>
											</table>
										</div>
										<div class="col text-center">
											<label>Nilai AP Inverse</label>
											<table id="" class="table table-striped">
												<tbody id="ap_inv`+i+`"></tbody>
											</table>
										</div>
										<div class="col text-center">
											<label>Nilai AQ Inverse</label>
											<table id="" class="table table-striped">
												<tbody id="aq_inv`+i+`"></tbody>
											</table>
										</div>
									</div>
									<div class="panel-body row m-0">
										<div class="col pl-0 text-center">
											<label>Nilai P Vector</label>
											<table id="" class="table table-striped">
												<tbody id="p_vector`+i+`"></tbody>
											</table>
										</div>
										<div class="col text-center">
											<label>Nilai Q Vector</label>
											<table id="" class="table table-striped">
												<tbody id="q_vector`+i+`"></tbody>
											</table>
										</div>
									</div>
									<div class="text-bold">Pembaharuan Nilai P dan Q</div>
									<div class="panel-body row m-0 mt-3">
										<div class="col pl-0 text-center">
											<label>Nilai P</label>
											<table id="" class="table table-striped">
												<tbody id="p`+i+`"></tbody>
											</table>
										</div>
										<div class="col text-center">
											<label>Nilai Q</label>
											<table id="" class="table table-striped">
												<tbody id="q`+i+`"></tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						`);
						//row 1
						for(j=0;j<sp.length;j++){
							$('#sp'+i).append('<tr id="sub-sp'+i+j+'"></tr>');
							for(k=0; k<sp[j].length; k++){
								$('#sub-sp'+i+j).append(`<td>`+Number(sp[j][k]).toFixed(3)+`</td>`)
							}
						}
						for(j=0;j<sq.length;j++){
							$('#sq'+i).append('<tr id="sub-sq'+i+j+'"></tr>');
							for(k=0; k<sq[j].length; k++){
								$('#sub-sq'+i+j).append(`<td>`+Number(sq[j][k]).toFixed(3)+`</td>`)
							}
						}
						for(j=0;j<mat_p.length;j++){
							$('#mat_p'+i).append('<tr id="sub-mat_p'+i+j+'"></tr>');
							for(k=0; k<mat_p[j].length; k++){
								$('#sub-mat_p'+i+j).append(`<td>`+Number(mat_p[j][k]).toFixed(3)+`</td>`)
							}
						}
						for(j=0;j<mat_q.length;j++){
							$('#mat_q'+i).append('<tr id="sub-mat_q'+i+j+'"></tr>');
							for(k=0; k<mat_q[j].length; k++){
								$('#sub-mat_q'+i+j).append(`<td>`+Number(mat_q[j][k]).toFixed(3)+`</td>`)
							}
						}
						//row 2
						for(j=0;j<ap.length;j++){
							$('#ap'+i).append('<tr id="sub-ap'+i+j+'"></tr>');
							for(k=0; k<ap[j].length; k++){
								$('#sub-ap'+i+j).append('<td id="sub-sub-ap'+i+j+k+'"></td>');
								for(l=0; l<ap[j][k].length; l++){
									$('#sub-sub-ap'+i+j+k).append('<div>'+Number(ap[j][k][l]).toFixed(3)+'</div>');
								}
							}
						}
						for(j=0;j<aq.length;j++){
							$('#aq'+i).append('<tr id="sub-aq'+i+j+'"></tr>');
							for(k=0; k<aq[j].length; k++){
								$('#sub-aq'+i+j).append('<td id="sub-sub-aq'+i+j+k+'"></td>');
								for(l=0; l<aq[j][k].length; l++){
									$('#sub-sub-aq'+i+j+k).append('<div>'+Number(aq[j][k][l]).toFixed(3)+'</div>');
								}
							}
						}
						for(j=0;j<ap_inv.length;j++){
							$('#ap_inv'+i).append('<tr id="sub-ap_inv'+i+j+'"></tr>');
							for(k=0; k<ap_inv[j].length; k++){
								$('#sub-ap_inv'+i+j).append('<td id="sub-sub-ap_inv'+i+j+k+'"></td>');
								for(l=0; l<ap_inv[j][k].length; l++){
									$('#sub-sub-ap_inv'+i+j+k).append('<div>'+Number(ap_inv[j][k][l]).toFixed(3)+'</div>');
								}
							}
						}
						for(j=0;j<aq_inv.length;j++){
							$('#aq_inv'+i).append('<tr id="sub-aq_inv'+i+j+'"></tr>');
							for(k=0; k<aq_inv[j].length; k++){
								$('#sub-aq_inv'+i+j).append('<td id="sub-sub-aq_inv'+i+j+k+'"></td>');
								for(l=0; l<aq_inv[j][k].length; l++){
									$('#sub-sub-aq_inv'+i+j+k).append('<div>'+Number(aq_inv[j][k][l]).toFixed(3)+'</div>');
								}
							}
						}
						//row 3
						for(j=0;j<p_vector.length;j++){
							$('#p_vector'+i).append('<tr id="sub-p_vector'+i+j+'"></tr>');
							for(k=0; k<p_vector[j].length; k++){
								$('#sub-p_vector'+i+j).append(`<td>`+Number(p_vector[j][k]).toFixed(3)+`</td>`)
							}
						}
						for(j=0;j<q_vector.length;j++){
							$('#q_vector'+i).append('<tr id="sub-q_vector'+i+j+'"></tr>');
							for(k=0; k<q_vector[j].length; k++){
								$('#sub-q_vector'+i+j).append(`<td>`+Number(q_vector[j][k]).toFixed(3)+`</td>`)
							}
						}
						//pembaharuan
						for(j=0;j<p.length;j++){
							$('#p'+i).append('<tr id="sub-p'+i+j+'"></tr>');
							for(k=0; k<p[j].length; k++){
								$('#sub-p'+i+j).append(`<td>`+Number(p[j][k]).toFixed(3)+`</td>`)
							}
						}
						for(j=0;j<q.length;j++){
							$('#q'+i).append('<tr id="sub-q'+i+j+'"></tr>');
							for(k=0; k<q[j].length; k++){
								$('#sub-q'+i+j).append(`<td>`+Number(q[j][k]).toFixed(3)+`</td>`)
							}
						}
						for(j=0;j<ap_new.length;j++){
							$('#ap_new'+i).append('<tr id="sub-ap_new'+i+j+'"></tr>');
							for(k=0; k<ap_new[j].length; k++){
								$('#sub-ap_new'+i+j).append('<td id="sub-sub-ap_new'+i+j+k+'"></td>');
								for(l=0; l<ap_new[j][k].length; l++){
									$('#sub-sub-ap_new'+i+j+k).append('<div>'+Number(ap_new[j][k][l]).toFixed(3)+'</div>');
								}
							}
						}
						for(j=0;j<aq_new.length;j++){
							$('#aq_new'+i).append('<tr id="sub-aq_new'+i+j+'"></tr>');
							for(k=0; k<aq_new[j].length; k++){
								$('#sub-aq_new'+i+j).append('<td id="sub-sub-aq_new'+i+j+k+'"></td>');
								for(l=0; l<aq_new[j][k].length; l++){
									$('#sub-sub-aq_new'+i+j+k).append('<div>'+Number(aq_new[j][k][l]).toFixed(3)+'</div>');
								}
							}
						}
					}
					//pq trans
					for(i=0;i<pq_trans.length;i++){
						$('#pq_trans').append('<tr id="sub_pq_trans'+i+'"><tr>');
						for(j=0;j<pq_trans[i].length;j++){
							$('#sub_pq_trans'+i).append('<td>'+Number(pq_trans[i][j]).toFixed(3)+'</td>')
						}
					}
					//mae
					$('#length_mae').text('/'+mae.length);
					$('#sum_mae').text(Number(sum_mae).toFixed(3));
					for(i=0;i<mae.length;i++){
						var nilai = mae[i].split('|')
						$('#mae').append('<span class="add_plus">(('+Number(nilai[0]).toFixed(3)+') - ('+Number(nilai[1]).toFixed(3)+'))<span>');
					}
				}
			});
		// console.log(data);
	})

</script>
@endsection