@extends('master.master_admin')
@section('content')
<div>
	<div class="title-admin">Akomodasi</div>
	<div class="content-admin">
		<div class="text-right mb-3">
			<a class="btn btn-app" href="{{url('/admin/add/akomodasi')}}">Tambah Akomodasi</a>
		</div>
		<table id="table_id" class="table text-center table-striped">
			<thead>
				<tr>
					<th scope="col">Nama Akomodasi</th>
					<th scope="col">Kota/Kabupaten</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($akomodasis as $akomodasi)
				<tr>
					<td>{{$akomodasi->judul}}</td>
					<td>{{$akomodasi->kota->name}}</td>
					<td>{{$akomodasi->created_at}}</td>
					<td>
						<a href="{{url('/detail/akomodasi',$akomodasi->id)}}" class="material-icons">visibility</a>
						<a href="{{url('admin/edit/akomodasi',$akomodasi->id)}}" class="material-icons">edit</a>
						<a dataid="{{$akomodasi->id}}" data-toggle="modal" data-target="#modalDelete" href="" class="material-icons delete">delete</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div id="modalDelete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus Akomodasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin menghapus Akomodasi ini ?</p>
      </div>
      <div class="modal-footer">
        <a class="btn btn-app" id="delete">Ya</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$('.delete').click(function () {
		var id = $(this).attr('dataid');
		$('#delete').attr('href',`{{url('/api/admin/delete/akomodasi/`+id+`')}}`);
	})
</script>
@endsection