@extends('master.master_admin')
@section('content')
<div id="formAdd">
	<div class="title-admin">Edit Slider</div>
	<div class="content-admin">
		<div class="row m-0 mb-3">
			<div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Nama Slider</div>
			<div class="col pr-0">
				<input type="" name="" id="judul" value="{{$slider->judul}}" placeholder="Nama Slider" class="form-control" style="max-width: 25rem">
			</div>
		</div>
   <div class="row m-0 mb-3">
     <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Gambar</div>
     <div class="col pr-0">
      <div class="d-inline-block text-center">
        <input name="image" type="file" accept="image/*" onchange="uploadimage(event)" id="imginput2" class="hidden" />
        <img id="imgview" src="{{asset('images/slider')}}/{{$slider->image}}" class="admin-view-sampul"/><br>
        <label class="mt-2 btn-change text-center" for="imginput2">
          <span class="btn border">Pilih Gambar</span>
        </label>
      </div>
    </div>
  </div>
  <div class="row m-0 mb-3">
   <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Deskripsi</div>
   <div class="col pr-0">
    <div id="desc"></div>
  </div>
</div>
<div class="text-right mb-5">
 <button class="btn btn-app" id="save">Simpan</button>
</div>
</div>
</div>
<input type="hidden" name="" value="{{$slider->id}}" id="id_slider">
<script type="text/javascript">
 var dataAll = [];
 var tmp=[];
 $('#slider').addClass('active');
 $('#desc').summernote('code', `{!!$slider->deskripsi!!}`);
$('#save').click(function () {
  if(file_imginput != ''){
    tmp.push(file_imginput);
  }
  dataAll = ({
    'judul': $('#judul').val(),
    'gambar': tmp,
    'deskripsi':$('#desc').summernote('code')
  })
    // statusForm = variabel terdapat di main.js
    if(statusForm == 0 && statusEdit == 1){
      alert('lengkapi Data');
    }
    else{
      $('#save').addClass('disabled');
      console.log(dataAll);
      var id = $('#id_slider').val();
      console.log(dataAll);
      $.ajax({
      url: "/api/admin/update/slider/"+id,
      type: "POST",
      data:  dataAll, 
      success:function(data){
        location.href="/admin/slider";
        console.log(data);
      }
    });
    }

  });
</script>
@endsection