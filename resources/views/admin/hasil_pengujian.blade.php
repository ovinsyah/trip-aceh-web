@extends('master.master_admin')
@section('content')
<div>
	<div class="title-admin mb-3">Hasil Pengujian</div>

	<div class="content-admin">
		<table id="table_id" class="table text-center table-striped">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">mae</th>
					<th scope="col">tmax</th>
					<th scope="col">k</th>
					<th scope="col">w0</th>
					<th scope="col">&lambda;</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($pengujians as $key => $pengujian)
				<tr>
					<td>{{$key+1}}</td>
					<td>{{$pengujian->mae}}</td>
					<td>{{$pengujian->tmax}}</td>
					<td>{{$pengujian->k}}</td>
					<td>{{$pengujian->w0}}</td>
					<td>{{$pengujian->lambda}}</td>
					<td>{{$pengujian->created_at}}</td>
					<td>
						<a href="{{url('/admin/detail-pengujian',$pengujian->id)}}"><i class="material-icons">visibility</i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection