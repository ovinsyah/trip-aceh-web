@extends('master.master_admin')
@section('content')
<div id="formAdd">
	<div class="title-admin">Tambah Akomodasi</div>
	<div class="content-admin">
		<div class="row m-0 mb-3">
			<div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Nama Akomodasi</div>
			<div class="col pr-0">
				<input type="" name="" id="judul" placeholder="Nama Akomodasi" class="form-control" style="max-width: 25rem">
			</div>
		</div>
		<div class="row m-0 mb-3">
			<div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Kota</div>
			<div class="col pr-0">
				<select class="form-control" id="kota" style="max-width: 25rem">
                   @foreach($kotas as $kota)
                   <option value="{{$kota->id}}">{{$kota->name}}</option>
                   @endforeach
               </select>
           </div>
       </div>
       <div class="row m-0 mb-3">
           <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Dekat dengan wisata</div>
           <div class="col pr-0">
            <div class="row m-0">
            @foreach($wisatas as $wisata)
              <div class="col-3 p-0 checkbok-wisata">
                <input type="checkbox" name="" value="{{$wisata->id}}" id="{{$wisata->id}}" class="list_wisata p-absolute">
                <label class="ml-4 cursor" for="{{$wisata->id}}">{{$wisata->judul}}</label>
              </div>
            @endforeach
            </div>
         </div>
     </div>
     <div class="row m-0 mb-3">
         <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Gambar</div>
         <div class="col pr-0">
          <div class="row m-0">
             <input class="hidden" id="add-img" accept="image/*" type="file" multiple/>
             <div class="col p-0">
                <label for="add-img">
                   <div class="btn-add-img">+</div>
               </label>
           </div>
       </div>
   </div>
</div>
<div class="row m-0 mb-3">
 <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Deskripsi</div>
 <div class="col pr-0">
  <div id="desc"></div>
</div>
</div>
<div class="text-right mb-5">
 <button class="btn btn-app" id="save">Simpan</button>
</div>
</div>
</div>
<script type="text/javascript">
   var fileImg = [];
   var dataAll = [];
   var wisata_terdekat = [];
   $('#akomodasi').addClass('active');
   $('#desc').summernote();
   if(window.File && window.FileList && window.FileReader)
   {
      $('#add-img').on('change',function (event) {
      var files = event.target.files; //FileList object
      for(var i = 0; i< files.length; i++)
      {
         var file = files[i];
         if(!file.type.match('image'))
            continue;
        var picReader = new FileReader();
        picReader.addEventListener("load",function(event){
            var picFile = event.target;
            $('#add-img').before("<div class='img-view'><img class='thumbnail-img' src='" + picFile.result + "'" +
               "title='" + picFile.name + "'/><div class='del-img'>Hapus</div></div>");
            $('.del-img').click(function(){
               $(this).parent().remove();
           });
        });
        picReader.readAsDataURL(file);
      }                               
      });
    }

$('#save').click(function () {
    $('.thumbnail-img').each(function (argument) {
        var img = $(this).attr('src');
        fileImg.push(img);
    });
    $('.list_wisata').each(function (argument) {
        if($(this).is(':checked')){
          wisata_terdekat.push($(this).val());
        }
    });
    dataAll = ({
        'judul': $('#judul').val(),
        'kota': $('#kota').val(),
        'wisata_terdekat':wisata_terdekat,
        'gambar': fileImg,
        'deskripsi':$('#desc').summernote('code')
    })
    // statusForm = variabel terdapat di main.js
    if(statusForm == 0){
        alert('lengkapi Data');
    }
    else if(fileImg == ''){
        alert('Gambar Tidak Ada');
    }
    else{
      $('#save').addClass('disabled');
    console.log(dataAll);
    $.ajax({
      url: "/api/admin/create/akomodasi",
      type: "POST",
      data:  dataAll, 
      success:function(data){
        location.href="/admin/akomodasi";
        console.log(data,"ini");
      }
    });
    }

});
</script>
@endsection