@extends('master.master_admin')
@section('content')
<div>
	<div class="title-admin mb-3">Ulasan</div>

	<div class="content-admin">
		<table id="table_id" class="table text-center table-striped">
			<thead>
				<tr>
					<th scope="col">Nama Pengguna</th>
					<th scope="col">Wisata</th>
					<th scope="col">Rating</th>
					<th scope="col">Ulasan</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($ulasans as $ulasan)
				@if($ulasan->rating != 0)
				<tr>
					<td>{{App\User::find($ulasan->user_id)->name}}</td>
					<td>{{App\Wisata::find($ulasan->wisata_id)->judul}}</td>
					<td>{{$ulasan->rating}}</td>
					<td>{{$ulasan->komentar}}</td>
					<td>{{$ulasan->created_at}}</td>
					<td>
						<a href="{{url('/detail/wisata',$ulasan->wisata_id)}}"><i class="material-icons">visibility</i></a>
						<a dataid="{{$ulasan->id}}" class="delete" href="" data-toggle="modal" data-target="#modalDelete"><i class="material-icons">delete</i></a>
					</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div id="modalDelete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus Ulasan</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin menghapus ulasan ini ?</p>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-app" id="delete">Ya</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$('.delete').click(function () {
		var id = $(this).attr('dataid');
		$('#delete').attr('href',`{{url('/admin/delete/ulasan/`+id+`')}}`);
	})
</script>
@endsection