@extends('master.master_admin')
@section('content')
<div id="formAdd">
	<div class="title-admin">Edit Wisata</div>
	<div class="content-admin">
		<div class="row m-0 mb-3">
			<div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Nama Wisata</div>
			<div class="col p-0">
				<input type="" name="" id="judul" placeholder="Nama Wisata" value="{{$wisata->judul}}" class="form-control" style="max-width: 25rem">
			</div>
		</div>
		<div class="row m-0 mb-3">
			<div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Kota</div>
			<div class="col p-0">
				<select class="form-control" id="kota" style="max-width: 25rem">
                   <option hidden="" value="{{$wisata->kota->id}}">{{$wisata->kota->name}}</option>
                   @foreach($kotas as $kota)
                   <option value="{{$kota->id}}">{{$kota->name}}</option>
                   @endforeach
               </select>
           </div>
       </div>
       <div class="row m-0 mb-3">
           <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Kategori</div>
           <div class="col p-0">
              <select class="form-control" id="kategori" style="max-width: 25rem">
                 <option hidden="" value="{{$wisata->kategori}}">{{$wisata->kategori == 1 ? 'Wisata Alam':'Wisata Sejarah'}}</option>
                 <option value="1">Wisata Alam</option>
                 <option value="2">Wisata Sejarah</option>
             </select>
         </div>
     </div>
     <div class="row m-0 mb-3">
         <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Gambar</div>
         <div class="col p-0">
          <div class="row m-0">
             <input class="hidden" id="add-img" accept="image/*" type="file" multiple/>
            @foreach($wisata->image()->where('jenis','Wisata')->get() as $image)
            <div class="img-view"><img class="thumbnail-img" src="{{asset('images/wisata')}}/{{$image->name}}"><div dataname="{{$image->name}}" class="del-img">Hapus</div></div>
            @endforeach
             <div class="col p-0">
                <label for="add-img">
                   <div class="btn-add-img">+</div>
               </label>
           </div>
       </div>
   </div>
</div>
<div class="row m-0 mb-3">
 <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Maps</div>
 <div class="col p-0">
  <span id="address">
    {{$wisata->alamat_maps}}
  </span>
  <div class="row m-0">
   <div class="col-6 p-0">
    <input type="text" class="form-control" id="location-text-box" name="alamat"  value="" />
</div>
<div class="col-6 text-right p-0">
    <a class="btn btn-app" id="set-address">Ambil Lokasi</a>
</div>
</div>
<p class="mt-3" id="map-canvas" style="width:100%; height:50rem;"></p>
</div>
</div>
<div class="row m-0 mb-3">
 <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Deskripsi</div>
 <div class="col p-0">
  <div id="desc"></div>
</div>
</div>
<div class="text-right mb-5">
 <button class="btn btn-app" id="save">Simpan</button>
</div>
</div>
</div>
<input type="hidden" name="" value="{{$wisata->latlng_maps}}" id="latlng">
<input type="hidden" name="" value="{{$wisata->alamat_maps}}" id="alamat">
<input type="hidden" name="" value="{{$wisata->id}}" id="id_wisata">
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOUtDxhLWJAwwg4qr9R70H1V37i83vWks&libraries=geometry,places"></script>
<script type="text/javascript">
  var tmplatlng = $('#latlng').val().split(",");
  var lat = parseFloat(tmplatlng[0]);
  var lng = parseFloat(tmplatlng[1]);
	var map,marker,loc,tmploc,jalan,kelurahan,kecamatan,kota,provinsi,negara,latlng,alamat_lengkap='';
  latlng = lat+','+lng;
  alamat_lengkap = $('#alamat').val();
   function initialize() {
      $("#set-address").click(function() {
         var geocoder = new google.maps.Geocoder;
         var infowindow = new google.maps.InfoWindow;
         geocodeLatLng(geocoder, infowindow);
     });

      var mapOptions = {
         zoom: 9
     };
     map = new google.maps.Map(document.getElementById('map-canvas'),
         mapOptions);

    // Get GEOLOCATION
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
      }, function() {
         var pos = new google.maps.LatLng(lat,lng);
         loc=pos;

         map.setCenter(pos);
         marker = new google.maps.Marker({
            position: pos,
            map: map,
            draggable: true
        });
         marker.addListener('dragend', function(event) {
            loc= event.latLng;
        });
     });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
}



function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
     var content = 'Error: The Geolocation service failed.';
 } else {
     var content = 'Error: Your browser doesn\'t support geolocation.';
 }
 loc= tmploc;
 map.setCenter(loc);
 var options = {
     map: map,
     position:loc ,
     content: content
 };

 map.setCenter(options.position);
 marker = new google.maps.Marker({
     position: options.position,
     map: map,
     draggable: true
 });
 marker.addListener('dragend', function(event) {
     console.log(event.latLng.lat(),event.latLng.lng())
     loc= event.latLng;
 });

}

  // get places auto-complete when user type in location-text-box
  var input = (document.getElementById('location-text-box'));


  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  marker = new google.maps.Marker({
   map: map,
   anchorPoint: new google.maps.Point(0, -29),
   draggable: true
});

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
   infowindow.close();
   marker.setVisible(false);
   var place = autocomplete.getPlace();
   if (!place.geometry) {
      return;
  }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
  } else {
      map.setCenter(place.geometry.location);
      map.setZoom(13); // Why 17? Because it looks good.
  }
  marker.setPosition(place.geometry.location);
  marker.setVisible(true);
  loc=place.geometry.location;

  var address = '';
  if (place.address_components) {
      address = [
      (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
  }

});
}
function geocodeLatLng(geocoder, infowindow) {
   geocoder.geocode({'location': loc}, function(results, status) {
      latlng = loc.lat()+','+loc.lng();
      console.log(latlng);
      var alamat = results[0].formatted_address.split(',');
      jalan =alamat[0];
      kelurahan =alamat[1];
      kecamatan =alamat[2];
      kota =alamat[3];
      provinsi =alamat[4];
      negara =alamat[5];
      alamat_lengkap = jalan+','+kelurahan+','+kecamatan+','+kota+','+provinsi+','+negara;
      $('#address').text(alamat_lengkap);
      console.log(alamat_lengkap);
      if (status === 'OK') {
         if (results[0]) {
            map.setZoom(13);
        } else {
            window.alert('No results found');
        }
    } else {
       window.alert('Geocoder failed due to: ' + status);
   }
});
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script type="text/javascript">
   var fileImg = [];
   var dataAll = [];
   var fileImgDelete = [];
   $('#wisata').addClass('active');
   $('#desc').summernote('code', `{!!$wisata->deskripsi!!}`);
   $('.del-img').click(function(){
       $(this).parent().remove();
       fileImgDelete.push($(this).attr('dataname'));
   });
   if(window.File && window.FileList && window.FileReader)
   {
      $('#add-img').on('change',function (event) {
      var files = event.target.files; //FileList object
      for(var i = 0; i< files.length; i++)
      {
         var file = files[i];
         if(!file.type.match('image'))
            continue;
        var picReader = new FileReader();
        picReader.addEventListener("load",function(event){
            var picFile = event.target;
            $('#add-img').before("<div class='img-view'><img class='thumbnail-img' src='" + picFile.result + "'" +
               "title='" + picFile.name + "'/><div class='del-img'>Hapus</div></div>");
            $('.del-img').click(function(){
               $(this).parent().remove();
           });
        });
        picReader.readAsDataURL(file);
    }                               
});
}

$('#save').click(function () {
    $('.thumbnail-img').each(function (argument) {
        var img = $(this).attr('src');
        fileImg.push(img);
    });
    dataAll = ({
        'judul': $('#judul').val(),
        'kota': $('#kota').val(),
        'kategori':$('#kategori').val(),
        'gambar': fileImg,
        'gambar_delete': fileImgDelete,
        'latlng_maps': latlng,
        'alamat_maps': alamat_lengkap,
        'deskripsi':$('#desc').summernote('code')
    })
    // statusForm = variabel terdapat di main.js
    if(statusForm == 0 && statusEdit == 1){
        alert('lengkapi Data');
    }
    else if(fileImg == ''){
        alert('Gambar Tidak Ada');
    }
    else if(alamat_lengkap == ''){
        alert('Alamat Tidak Ada');
    }
    else{
      $('#save').addClass('disabled');
    console.log(dataAll);
      kirim(dataAll);
    }

});
function kirim(data) {
  var id = $('#id_wisata').val();
    console.log('Ini data : ',data)
    $.ajax({
      url: "/api/admin/update/wisata/"+id,
      type: "POST",
      data:  data, 
      success:function(data){
        location.href="/admin/wisata";
        console.log(data);
      }
    });
  }
</script>
@endsection