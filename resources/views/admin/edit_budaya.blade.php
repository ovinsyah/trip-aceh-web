@extends('master.master_admin')
@section('content')
<div id="formAdd">
	<div class="title-admin">Edit Budaya</div>
	<div class="content-admin">
		<div class="row m-0 mb-3">
			<div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Nama Budaya</div>
			<div class="col pr-0">
				<input type="" name="" id="judul" value="{{$budaya->judul}}" placeholder="Nama Budaya" class="form-control" style="max-width: 25rem">
			</div>
		</div>
		<div class="row m-0 mb-3 hidden">
			<div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Kota</div>
			<div class="col pr-0">
				<select class="form-control" id="kota" style="max-width: 25rem">
         <option hidden="" value="{{$budaya->kota->id}}">{{$budaya->kota->name}}</option>
         @foreach($kotas as $kota)
         <option value="{{$kota->id}}">{{$kota->name}}</option>
         @endforeach
       </select>
     </div>
   </div>
   <div class="row m-0 mb-3">
         <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Gambar</div>
         <div class="col pr-0">
          <div class="row m-0">
             <input class="hidden" id="add-img" accept="image/*" type="file" multiple/>
            @foreach($budaya->image()->where('jenis','Budaya')->get() as $image)
            <div class="img-view"><img class="thumbnail-img" src="{{asset('images/budaya')}}/{{$image->name}}"><div dataname="{{$image->name}}" class="del-img">Hapus</div></div>
            @endforeach
             <div class="col p-0">
                <label for="add-img">
                   <div class="btn-add-img">+</div>
               </label>
           </div>
       </div>
   </div>
</div>
<div class="row m-0 mb-3">
 <div class="col p-0 pt-2 font-14 text-bold" style="max-width: 14rem">Deskripsi</div>
 <div class="col pr-0">
  <div id="desc"></div>
</div>
</div>
<div class="text-right mb-5">
 <button class="btn btn-app" id="save">Simpan</button>
</div>
</div>
</div>
<input type="hidden" name="" value="{{$budaya->id}}" id="id_budaya">
<script type="text/javascript">
 var fileImg = [];
 var dataAll = [];
 var fileImgDelete = [];
 $('#budaya').addClass('active');
 $('#desc').summernote('code', `{!!$budaya->deskripsi!!}`);
 $('.del-img').click(function(){
       $(this).parent().remove();
       fileImgDelete.push($(this).attr('dataname'));
   });
 if(window.File && window.FileList && window.FileReader)
 {
  $('#add-img').on('change',function (event) {
      var files = event.target.files; //FileList object
      for(var i = 0; i< files.length; i++)
      {
       var file = files[i];
       if(!file.type.match('image'))
        continue;
      var picReader = new FileReader();
      picReader.addEventListener("load",function(event){
        var picFile = event.target;
        $('#add-img').before("<div class='img-view'><img class='thumbnail-img' src='" + picFile.result + "'" +
         "title='" + picFile.name + "'/><div class='del-img'>Hapus</div></div>");
        $('.del-img').click(function(){
         $(this).parent().remove();
       });
      });
      picReader.readAsDataURL(file);
    }                               
  });
}

$('#save').click(function () {
  $('.thumbnail-img').each(function (argument) {
    var img = $(this).attr('src');
    fileImg.push(img);
  });
  dataAll = ({
    'judul': $('#judul').val(),
    'kota': $('#kota').val(),
    'gambar': fileImg,
    'gambar_delete': fileImgDelete,
    'deskripsi':$('#desc').summernote('code')
  })
    // statusForm = variabel terdapat di main.js
    if(statusForm == 0){
      alert('lengkapi Data');
    }
    else if(fileImg == ''){
      alert('Gambar Tidak Ada');
    }
    else{
      $('#save').addClass('disabled');
      var id = $('#id_budaya').val();
      console.log(dataAll);
      $.ajax({
      url: "/api/admin/update/budaya/"+id,
      type: "POST",
      data:  dataAll, 
      success:function(data){
        location.href="/admin/budaya";
        console.log(data);
      }
    });
    }

  });
</script>
@endsection