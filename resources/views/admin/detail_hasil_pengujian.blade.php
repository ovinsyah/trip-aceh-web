@extends('master.master_admin')
@section('content')
<div>
	<div class="title-admin mb-3">Detail Hasil Pengujian</div>

	<div class="content-admin">
		<div class="row m-0">
			<div class="col p-0 text-justify">
				@foreach($pengujian->detail as $detail)
					<span class="add_plus">(({{$detail->rating}}) - ({{$detail->hasil}}))</span>
				@endforeach
			</div>
			<div class="col text-center" style="max-width: 17rem;padding-top: 50%;font-size: 30px;">
				<div>/ {{count($pengujian->detail)}}</div>
				= {{$pengujian->mae}}
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#hasil-pengujian').addClass('active');
</script>
@endsection