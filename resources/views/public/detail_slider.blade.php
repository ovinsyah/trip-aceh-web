@extends('master.master_main')
@section('content')
<div class="container-app mt-4">
	<img src="{{asset('images/slider')}}/{{$slider->image}}" style="width:  100%;height: 40rem;object-fit:  cover;">
	<div class="section-app mt-3">
		<div class="title-section-app border-bottom">{{$slider->judul}}</div>
		<div class="font-14 mt-3">
			<p>{!!$slider->deskripsi!!}</p>
		</div>
	</div>
</div>
@endsection