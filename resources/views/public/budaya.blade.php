@extends('master.master_main')
@section('content')
<div class="container-app mt-4">
	<div class="row m-0 mb-3">
		<div class="col p-0">
			<div class="text-bold font-20">Budaya Aceh</div>
		</div>
<!-- 		<div class="col p-0 text-right">
			<select class="form-control d-inline-block" style="max-width: 15rem">
				<option>Semua Lokasi</option>
				@foreach($kotas as $kota)
				<option value="{{$kota->id}}">{{$kota->name}}</option>
				@endforeach
			</select>
		</div> -->
	</div>
	<div class="row">
		@foreach($budayas as $budaya)
		@php($image=$budaya->image()->where('jenis','budaya')->get())
		<div class="col-md-3 col-6">
			<a href="{{url('detail/budaya',$budaya->id)}}">
				<div class="thumb-img">
					@if(count($image)== 0)
					<img src="{{asset('img/no-image-wisata.png')}}">
					@else
					<img src="{{asset('images/budaya')}}/{{$image[0]->name}}">
					@endif
				</div>
			</a>
				<div class="thumb-title-2 text-bold font-16">{{$budaya->judul}}</div>
		</div>
		@endforeach
	</div>
	<div class="text-right">
			{{$budayas->links()}}
		</div>
</div>
@endsection