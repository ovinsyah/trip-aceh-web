@extends('master.master_main')
@section('content')
<div class="container-app mt-4">
	<div class="row m-0 mb-3">
		<div class="col p-0">
			<div class="text-bold font-20">Akomodasi di Aceh</div>
		</div>
		<div class="col p-0 text-right">
			<select class="form-control d-inline-block" style="max-width: 15rem" id="kota">
				<option>{{$key}}</option>
				@if($key!="Semua Lokasi")
				<option value="0">Semua Lokasi</option>
				@endif
				@foreach($kotas as $kota)
				@if($kota->name!=$key)
				<option value="{{$kota->id}}">{{$kota->name}}</option>
				@endif
				@endforeach
			</select>
		</div>
	</div>
	<div class="row">
		@foreach($akomodasis as $akomodasi)
		@php($image=$akomodasi->image()->where('jenis','Akomodasi')->get())
		<div class="col-md-4 col-lg-3 col-6">
			<a href="{{url('detail/akomodasi',$akomodasi->id)}}">
				<div class="thumb-img">
					@if(count($image)== 0)
					<img src="{{asset('img/no-image-wisata.png')}}">
					@else
					<img src="{{asset('images/Akomodasi')}}/{{$image[0]->name}}">
					@endif
				</div>
			</a>
			<div class="thumb-title-2">
				<div class="text-bold font-16" style="height: 23px">
					{{$akomodasi->judul}}
				</div>
				<div class="thumb-rate">
					<?php 
					$sumrate = 0;
					$countrate = 0;
					?>
					@foreach($akomodasi->ulasan as $ulasan)
					@if($ulasan->rating != 0)
					<?php 
					$sumrate += $ulasan->rating;
					$countrate ++;
					?>
					@endif
					@endforeach

					@if($sumrate != 0)
					@php($sumrate = round($sumrate/$countrate))
					@endif

					@for($i=0; $i < $sumrate; $i++)
					<i class="material-icons rate">radio_button_checked</i>
					@endfor
					@for($i=0; $i < (5-$sumrate); $i++)
					<i class="material-icons rate">radio_button_unchecked</i>
					@endfor
					<span class="ml-2">{{$countrate}}/Ulasan</span>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<div class="text-right">
		{{$akomodasis->links()}}
	</div>
</div>

<script type="text/javascript">
	$('#kota').on('change',function () {
		console.log($(this).val(),"kota")
		location.href="/filter/akomodasi/"+$(this).val();
	})

</script>
@endsection