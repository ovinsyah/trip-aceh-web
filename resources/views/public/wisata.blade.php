@extends('master.master_main')
@section('content')
<div class="container-app mt-4">
	@guest
	@else
	<div class="row m-0 mb-2">
		<div class="col p-0">
			<div class="text-bold font-20">Rekomendasi Wisata</div>
		</div>
		<div class="col p-0 text-right">
			<a href="{{url('recommendation-wisata')}}" class="btn btn-app">Lihat Semua</a>
		</div>
	</div>
	<div class="row">
		@foreach($wisataRekomen as $wisata)
		@php($img=$wisata->image()->where('jenis',"Wisata")->get())
		<div class="col-md-3 col-6 mb-4">
			<a href="{{url('detail/wisata',$wisata->id)}}">
				<div class="thumb-img">
					@if(count($img)== 0)
					<img src="{{asset('img/no-image-wisata.png')}}">
					@else
					<img src="{{asset('images/wisata')}}/{{$img[0]->name}}">
					@endif
				</div>
			</a>
			<a href="{{url('detail/wisata',$wisata->id)}}">
				<div class="thumb-title font-18">{{$wisata->judul}}</div>
			</a>
			<div class="thumb-rate">
				<?php 
					$sumrate = 0;
					$countrate = 0;
				?>
				@foreach($wisata->ulasan as $ulasan)
					@if($ulasan->rating != 0)
						<?php 
							$sumrate += $ulasan->rating;
							$countrate ++;
						?>
					@endif
				@endforeach

				@if($sumrate != 0)
					@php($sumrate = round($sumrate/$countrate))
				@endif

				@for($i=0; $i < $sumrate; $i++)
					<i class="material-icons rate">radio_button_checked</i>
				@endfor
				@for($i=0; $i < (5-$sumrate); $i++)
					<i class="material-icons rate">radio_button_unchecked</i>
				@endfor
				<span class="color-app ml-2">{{$countrate}}/Ulasan</span>
			</div>
			<div class="thumb-desc">
				{!!$wisata->deskripsi!!}
			</div>
		</div>
		@endforeach
	</div>
	@endguest
	<div class="row m-0 mb-4 mt-5">
		<div class="col p-0">
			<div class="text-bold font-20">Wisata Aceh</div>
		</div>
		<div class="col p-0 text-right">
			<select class="form-control d-inline-block mr-2" style="max-width: 15rem" id="kota">
				<option value="{{$id}}">{{($id==0)?'Semua Lokasi':App\kota_aceh::find($id)->name}}</option>
				@if($id!=0)
				<option value="0">Semua Lokasi</option>
				@endif
				@foreach($kotas as $kota)
				@if($kota->id!=$id)
					<option value="{{$kota->id}}">{{$kota->name}}</option>
				@endif
				@endforeach
			</select>
			@php($arrK=['Semua Kategori','Wisata Alam','Wisata Sejarah'])
			<select class="form-control d-inline-block" style="max-width: 15rem" id="kategori">

				<option value="{{$kategori}}">{{$arrK[$kategori]}}</option>
				@for($i=0;$i< 3;$i++)
				@if($i!=$kategori)
				<option value="{{$i}}">{{$arrK[$i]}}</option>
				@endif
				@endfor
			</select>
		</div>
	</div>
	<div class="row">
		@foreach($wisatas as $wisata)
		@php($img=$wisata->image()->where('jenis',"Wisata")->get())
		<div class="col-md-3 col-6 mb-5">
			<a href="{{url('detail/wisata',$wisata->id)}}">
				<div class="thumb-img">
					@if(count($img)== 0)
					<img src="{{asset('img/no-image-wisata.png')}}">
					@else
					<img src="{{asset('images/wisata')}}/{{$img[0]->name}}">
					@endif
				</div>
			</a>
			<a href="{{url('detail/wisata',$wisata->id)}}">
				<div class="thumb-title font-18">{{$wisata->judul}}</div>
			</a>
			<div class="thumb-rate">
				<?php 
					$sumrate = 0;
					$countrate = 0;
				?>
				@foreach($wisata->ulasan as $ulasan)
					@if($ulasan->rating != 0)
						<?php 
							$sumrate += $ulasan->rating;
							$countrate ++;
						?>
					@endif
				@endforeach

				@if($sumrate != 0)
					@php($sumrate = round($sumrate/$countrate))
				@endif

				@for($i=0; $i < $sumrate; $i++)
					<i class="material-icons rate">radio_button_checked</i>
				@endfor
				@for($i=0; $i < (5-$sumrate); $i++)
					<i class="material-icons rate">radio_button_unchecked</i>
				@endfor
				<span class="color-app ml-2">{{$countrate}}/Ulasan</span>
			</div>
			<div class="thumb-desc">
				{!!$wisata->deskripsi!!}
			</div>
		</div>
		@endforeach
	</div>
		<div class="text-right">
			{{$wisatas->links()}}
		</div>
</div>

<script type="text/javascript">
$('#kota').on('change',function () {
	console.log($(this).val(),$('#kategori').val(),"kota")
     location.href="/filter/wisata/"+$(this).val()+'/'+$('#kategori').val();
})
$('#kategori').on('change',function () {
	console.log($(this).val(),$('#kota').val(),"kategori")
    location.href="/filter/wisata/"+$('#kota').val()+'/'+$(this).val();
})

</script>
@endsection