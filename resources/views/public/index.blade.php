@extends('master.master_main')
@section('content')
<!-- Search -->
<div class="input-group search-app">
	<input type="text" class="form-control form-app" placeholder="Cari tempat wisata" id="search">
	<span class="input-group-btn">
		<button class="btn btn-search-app disabled" id="cari">Cari</button>
	</span>
</div>
<!-- End Search -->
<!-- Slider -->
<div id="myCarousel" class="carousel slide slider-app" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
			@foreach($sliders as $key=> $slider)
			<li data-target="#myCarousel" data-slide-to="{{$key}}" class="{{$key == 0 ? 'active':''}}"></li>
			@endforeach
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			@foreach($sliders as $key=> $slider)
			<div class="item {{$key == 0 ? 'active':''}}">
				<img src="{{asset('images/slider')}}/{{$slider->image}}">
				<div class="carousel-caption">
					<h3><a class="text-white" href="{{url('detail/slider',$slider->id)}}">{{$slider->judul}}</a></h3>
				</div>
			</div>
			@endforeach
		</div>

	<!-- Left and right controls -->
	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<!-- End Slider -->
<div class="container-app mt-4">
	@if(Auth::user())
	<div class="row m-0 mb-2">
		<div class="col p-0">
			<div class="text-bold font-20">Rekomendasi Wisata</div>
		</div>
		<div class="col p-0 text-right">
			<a href="{{url('recommendation-wisata')}}" class="btn btn-app">Lihat Semua</a>
		</div>
	</div>
	<div class="row">
		@foreach($recomends as $recomend)
		<div class="col-md-3 col-6 mb-4">
			<a href="{{url('detail/wisata',$recomend->id)}}">
				<div class="thumb-img">
					@if(count($recomend->image)== 0)
					<img src="{{asset('img/no-image-wisata.png')}}">
					@else
					<img src="{{asset('images/wisata')}}/{{$recomend->image[0]->name}}">
					@endif
				</div>
			</a>
			<a href="{{url('detail/wisata',$recomend->id)}}">
				<div class="thumb-title font-18">{{$recomend->judul}}</div>
			</a>
			<div class="thumb-rate">
				<?php 
					$sumrate = 0;
					$countrate = 0;
				?>
				@foreach($recomend->ulasan as $ulasan)
					@if($ulasan->rating != 0)
						<?php 
							$sumrate += $ulasan->rating;
							$countrate ++;
						?>
					@endif
				@endforeach

				@if($sumrate != 0)
					@php($sumrate = round($sumrate/$countrate))
				@endif

				@for($i=0; $i < $sumrate; $i++)
					<i class="material-icons rate">radio_button_checked</i>
				@endfor
				@for($i=0; $i < (5-$sumrate); $i++)
					<i class="material-icons rate">radio_button_unchecked</i>
				@endfor
				<span class="color-app ml-2">{{$countrate}}/Ulasan</span>
			</div>
			<div class="thumb-desc">
				{!!$recomend->deskripsi!!}
			</div>
		</div>
		@endforeach
	</div>
	@endif
	<div class="row m-0 mb-2 mt-5">
		<div class="col p-0">
			<div class="text-bold font-20">Top Rate Wisata</div>
		</div>
		<div class="col p-0 text-right">
			<a href="{{url('top-rate-wisata')}}" class="btn btn-app">Lihat Semua</a>
		</div>
	</div>
	<div class="row">
		@foreach($toprates as $top)
		<div class="col-md-3 col-6 mb-4">
			<a href="{{url('detail/wisata',$top->id)}}">
				<div class="thumb-img">
					@if(count($top->image)== 0)
					<img src="{{asset('img/no-image-wisata.png')}}">
					@else
					<img src="{{asset('images/wisata')}}/{{$top->image[0]->name}}">
					@endif
				</div>
			</a>
			<a href="{{url('detail/wisata',$top->id)}}">
				<div class="thumb-title font-18">{{$top->judul}}</div>
			</a>
			<div class="thumb-rate">
				<?php 
					$sumrate = 0;
					$countrate = 0;
				?>
				@foreach($top->ulasan as $ulasan)
					@if($ulasan->rating != 0)
						<?php 
							$sumrate += $ulasan->rating;
							$countrate ++;
						?>
					@endif
				@endforeach

				@if($sumrate != 0)
					@php($sumrate = round($sumrate/$countrate))
				@endif

				@for($i=0; $i < $sumrate; $i++)
					<i class="material-icons rate">radio_button_checked</i>
				@endfor
				@for($i=0; $i < (5-$sumrate); $i++)
					<i class="material-icons rate">radio_button_unchecked</i>
				@endfor
				<span class="color-app ml-2">{{$countrate}}/Ulasan</span>
			</div>
			<div class="thumb-desc">
				{!!$top->deskripsi!!}
			</div>
		</div>
		@endforeach
	</div>

	<div class="row m-0 mb-2 mt-5">
		<div class="col p-0">
			<div class="text-bold font-20">Akomodasi di Aceh</div>
		</div>
		<div class="col p-0 text-right">
			<a href="{{url('akomodasi')}}" class="btn btn-app">Lihat Semua</a>
		</div>
	</div>
	<div class="row">
		@foreach($akomodasis as $akomodasi)
		@php($image=$akomodasi->image()->where('jenis','Akomodasi')->get())
		<div class="col-md-3 col-6">
			<a href="{{url('detail/akomodasi',$akomodasi->id)}}">
				<div class="thumb-img">
					@if(count($image)== 0)
					<img src="{{asset('img/no-image-wisata.png')}}">
					@else
					<img src="{{asset('images/Akomodasi')}}/{{$image[0]->name}}">
					@endif
				</div>
			</a>
				<div class="thumb-title-2">
					<div class="text-bold font-16" style="height: 23px">
						{{$akomodasi->judul}}
					</div>
					<div class="thumb-rate">
				<?php 
					$sumrate = 0;
					$countrate = 0;
				?>
				@foreach($akomodasi->ulasan as $ulasan)
					@if($ulasan->rating != 0)
						<?php 
							$sumrate += $ulasan->rating;
							$countrate ++;
						?>
					@endif
				@endforeach

				@if($sumrate != 0)
					@php($sumrate = round($sumrate/$countrate))
				@endif

				@for($i=0; $i < $sumrate; $i++)
					<i class="material-icons rate">radio_button_checked</i>
				@endfor
				@for($i=0; $i < (5-$sumrate); $i++)
					<i class="material-icons rate">radio_button_unchecked</i>
				@endfor
				<span class="ml-2">{{$countrate}}/Ulasan</span>
			</div>
				</div>
		</div>
		@endforeach
	</div>

	<div class="row m-0 mb-2 mt-5">
		<div class="col p-0">
			<div class="text-bold font-20">Budaya Aceh</div>
		</div>
		<div class="col p-0 text-right">
			<a href="{{url('budaya')}}" class="btn btn-app">Lihat Semua</a>
		</div>
	</div>
	<div class="row">
		@foreach($budayas as $budaya)
		@php($image=$budaya->image()->where('jenis','budaya')->get())
		<div class="col-md-3 col-6">
			<a href="{{url('detail/budaya',$budaya->id)}}">
				<div class="thumb-img">
					@if(count($image)== 0)
					<img src="{{asset('img/no-image-wisata.png')}}">
					@else
					<img src="{{asset('images/budaya')}}/{{$image[0]->name}}">
					@endif
				</div>
			</a>
				<div class="thumb-title-3 text-bold font-16">{{$budaya->judul}}</div>
		</div>
		@endforeach
	</div>
</div>
<script type="text/javascript">
	$('#search').on('click change keyup',function () {
		if($('#search').val() == ''){
			$('#cari').addClass('disabled');
		}
		else{
			$('#cari').removeClass('disabled');
		}
	})
	$('#cari').on('click',function () {
		location.href="/search/wisata/"+$('#search').val();
	});
	var options = {
  url: '/wisata_all',
  list: {
      match: {
        enabled: true
      }
    }
};

$("#search").easyAutocomplete(options);
</script>
@endsection