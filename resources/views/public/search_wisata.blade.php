@extends('master.master_main')
@section('content')
<div class="container-app mt-4">
	<div class="font-24 mb-5 mt-5"><i>Pencarian Wisata : {{$key}}</i></div>
	@if(count($wisatas) == 0)
		<div class="text-center"><img src="{{asset('img/wisata-not-found.png')}}"></div>
	@else
	<div class="row">
		@foreach($wisatas as $wisata)
		<div class="col-md-3 col-6 mb-4">
			<a href="{{url('detail/wisata',$wisata->id)}}">
				<div class="thumb-img">
					@if(count($wisata->image)== 0)
					<img src="{{asset('img/no-image-wisata.png')}}">
					@else
					<img src="{{asset('images/wisata')}}/{{$wisata->image[0]->name}}">
					@endif
				</div>
			</a>
			<a href="{{url('detail/wisata',$wisata->id)}}">
				<div class="thumb-title font-18">{{$wisata->judul}}</div>
			</a>
			<div class="thumb-rate">
				<?php 
					$sumrate = 0;
					$countrate = 0;
				?>
				@foreach($wisata->ulasan as $ulasan)
					@if($ulasan->rating != 0)
						<?php 
							$sumrate += $ulasan->rating;
							$countrate ++;
						?>
					@endif
				@endforeach

				@if($sumrate != 0)
					@php($sumrate = round($sumrate/$countrate))
				@endif

				@for($i=0; $i < $sumrate; $i++)
					<i class="material-icons rate">radio_button_checked</i>
				@endfor
				@for($i=0; $i < (5-$sumrate); $i++)
					<i class="material-icons rate">radio_button_unchecked</i>
				@endfor
				<span class="color-app ml-2">{{$countrate}}/Ulasan</span>
			</div>
			<div class="thumb-desc">
				{!!$wisata->deskripsi!!}
			</div>
		</div>
		@endforeach
	</div>
	@endif
</div>
@endsection