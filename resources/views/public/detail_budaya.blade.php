@extends('master.master_main')
@section('content')
<div class="container-app mt-4">
	<!-- Slider -->
	<div id="myCarousel" class="carousel slide slider-app" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			@foreach($budaya->image()->where('jenis','budaya')->get() as $key=> $image)
			<li data-target="#myCarousel" data-slide-to="{{$key}}" class="{{$key == 0 ? 'active':''}}"></li>
			@endforeach
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			@foreach($budaya->image()->where('jenis','budaya')->get() as $key=> $image)
			<div class="item {{$key == 0 ? 'active':''}}">
				<img src="{{asset('images/budaya')}}/{{$image->name}}">
			</div>
			@endforeach
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<!-- End Slider -->
	<div class="section-app mt-3">
		<div class="title-section-app border-bottom">{{$budaya->judul}}</div>
		<div class="font-14 mt-3">
			<p>{!!$budaya->deskripsi!!}</p>
		</div>
	</div>
</div>
@endsection