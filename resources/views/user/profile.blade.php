@extends('master.master_main')
@section('content')
<div class="container-app mt-4">
	<div class="col-profile-user">
		<div class="text-right">
			<a href="{{url('user/edit/profile')}}"><i class="material-icons">edit</i></a>
		</div>
		<div class="text-center">
			@if(Auth::user()->image == '')
              <img src="{{asset('img/profile.png')}}" class="profile-user">
            @else
             <img src="{{asset('images/user/')}}/{{Auth::user()->image}}" class="profile-user">
            @endif
			<div class="font-20 text-bold mt-2">
				{{Auth::user()->name}}
			</div>
			<div class="font-16">
				{{Auth::user()->email}}
			</div>
		</div>
	</div>
</div>
@endsection