@extends('master.master_main')
@section('content')
<div class="container-app mt-4">
	<div class="col-profile-user">
		<form action="{{url('/user/update/profile')}}" method="post" enctype="multipart/form-data">
			{{csrf_field()}}
		<div class="text-center">
			<div class="d-inline-block text-center">
		        <input name="image" type="file" accept="image/*" onchange="uploadimage(event)" id="imginput" class="hidden" />
		        @if(Auth::user()->image == '')
	              <img id="imgview" src="{{asset('img/profile.png')}}" class="profile-user"><br>
	            @else
	             <img src="{{asset('images/user/')}}/{{Auth::user()->image}}" class="profile-user"><br>
	            @endif
		        <label class="mt-2 btn-change text-center" for="imginput">
		          <span class="btn border">Pilih Gambar</span>
		        </label>
		      </div>
			<div class="mt-2">
				<div class="text-left text-bold mb-2">Nama Lengkap</div>
				<input type="" name="nama" value="{{Auth::user()->name}}" class="form-control">
			</div>
			<div class="text-right mt-3">
				<button type="submit" class="btn btn-app">Simpan</button>
			</div>
		</div>
		</form>
	</div>
</div>
@endsection