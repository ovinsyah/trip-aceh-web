@extends('master.master_main')
@section('content')
<div class="container-app mt-4" id="formApp">
	<div class="col-profile-user">
		<form action="{{url('/user/update/password')}}" method="post">
			{{csrf_field()}}
			<div class="text-center">
				<div class="d-inline-block text-center">
					@if(Auth::user()->image == '')
		              <img src="{{asset('img/profile.png')}}" class="profile-user">
		            @else
		             <img src="{{asset('images/user/')}}/{{Auth::user()->image}}" class="profile-user">
		            @endif
					<div class="font-20 text-bold mt-2">
						{{Auth::user()->name}}
					</div>
					<div class="font-16">
						{{Auth::user()->email}}
					</div>
				</div>
				<div class="mt-2">
					<div class="text-left text-bold mb-2">Password Lama</div>
					<input type="password" name="old" class="form-control">
				</div>
				<div class="mt-2">
					<div class="text-left text-bold mb-2">Password Baru</div>
					<input type="password" name="new" class="form-control" id="pass">
				</div>
				<div class="mt-2">
					<div class="text-left text-bold mb-2">Konfirmasi Password</div>
					<input type="password"  class="form-control" id="repass">
				</div>
				<div class="text-right mt-3">
					<button type="submit" class="btn btn-app disabled" id="save">Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$('#formApp').on('click change keyup',function () {
		$('#pass,#repass').removeClass('form-error');
		$('#save').removeClass('disabled');
		if($('#pass').val() != $('#repass').val() || $('#pass').val().length <6 || $('#repass').val().length <6){
			$('#pass,#repass').addClass('form-error');
			$('#save').addClass('disabled');
		}
	});
</script>
@endsection