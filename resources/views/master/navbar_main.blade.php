<nav class="navbar navbar-default navbar-app">
  <div class="container-fluid container-app p-0">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><span class="color-app font-20">Trip<b>Aceh</b></span></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav menu-app">
        <li><a class="{{ request()->is('wisata') ? 'active' : '' }}" href="{{url('/wisata')}}">Wisata</a></li>
        <li><a class="{{ request()->is('budaya') ? 'active' : '' }}" href="{{url('/budaya')}}">Budaya</a></li>
        <li><a class="{{ request()->is('akomodasi') ? 'active' : '' }}" href="{{url('/akomodasi')}}">Akomodasi</a></li>
      </ul>
      @guest
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/register')}}">Daftar</a></li>
        <li><a href="{{url('/login')}}">Masuk</a></li>
      </ul>
      @else
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 3px 0px 3px 12px;background: none;box-shadow: none;">
            @if(Auth::user()->image == '')
              <img src="{{asset('img/profile.png')}}" class="profile-navbar-app">
            @else
             <img src="{{asset('images/user/')}}/{{Auth::user()->image}}" class="profile-navbar-app">
            @endif
          </a>
          <ul class="dropdown-menu">
            <li><a href="{{url('/user/profile')}}">Profile</a></li>
            <li><a href="{{url('/user/account')}}">Akun</a></li>
            <li role="separator" class="divider"></li>
            <li>
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
          </ul>
        </li>
      </ul>
      @endguest
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>