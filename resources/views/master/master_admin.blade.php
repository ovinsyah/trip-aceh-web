<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>TripAceh</title>
	<meta name="description" content="">
	<meta name="author" content="">
	
</head>
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-grid.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.dataTables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/import.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/admin.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/material-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('font-awesome/css/font-awesome.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-datetimepicker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/summernote.css')}}">

<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('js/Chart.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/summernote.js')}}"></script>


<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="row m-0 pt-4 pb-3">
				<div class="col p-0">
					<span class="font-18 text-bold mr-3">Admin</span>
					<button type="button" id="sidebarCollapse" class="btn btn-app navbar-btn m-0">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
				</div>
				<div class="col p-0 pt-2 text-right">
					<a href="{{url('admin/logout')}}">Logout</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="wrapper">
		<nav id="sidebar">
			<div class="sidebar-header text-center">
				<h3>TripAceh</h3>
			</div>
			<ul class="list-unstyled components">
				<li id="dashboard" class="{{ request()->is('admin') ? 'active' : '' }}"><a href="{{url('/admin/')}}"><i class="material-icons">assessment</i> Dashboard</a></li>
				<li id="user" class="{{ request()->is('admin/user') ? 'active' : '' }}"><a href="{{url('/admin/user')}}"><i class="material-icons">assignment_ind</i> User</a></li>
				<li id="wisata" class="{{ request()->is('admin/wisata') ? 'active' : '' }}"><a href="{{url('/admin/wisata')}}"><i class="material-icons">landscape</i> Wisata</a></li>
				<li id="akomodasi" class="{{ request()->is('admin/akomodasi') ? 'active' : '' }}"><a href="{{url('/admin/akomodasi')}}"><i class="material-icons">hotel</i> Akomodasi</a></li>
				<li id="budaya" class="{{ request()->is('admin/budaya') ? 'active' : '' }}"><a href="{{url('/admin/budaya')}}"><i class="material-icons">local_library</i> Budaya</a></li>
				<li id="slider" class="{{ request()->is('admin/slider') ? 'active' : '' }}"><a href="{{url('/admin/slider')}}"><i class="material-icons">info</i> Slider</a></li>
				<li id="ulasan" class="{{ request()->is('admin/ulasan') ? 'active' : '' }}"><a href="{{url('/admin/ulasan')}}"><i class="material-icons">grade</i> Ulasan</a></li>
				<li id="pengujian" class="{{ request()->is('admin/pengujian') ? 'active' : '' }}"><a href="{{url('/admin/pengujian')}}"><i class="material-icons">security</i> Pengujian</a></li>
				<li id="hasil-pengujian" class="{{ request()->is('admin/hasil-pengujian') ? 'active' : '' }}"><a href="{{url('/admin/hasil-pengujian')}}"><i class="material-icons">assessment</i> Hasil Pengujian</a></li>
			</ul>
		</nav>
		<div id="content">
			@yield('content')
		</div>
	</div>
	<script type="text/javascript">
		$('#table_id').DataTable();
		$('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                 });
	</script>
</body>
</html>