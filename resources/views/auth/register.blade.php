<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TripAceh</title>
    <meta name="description" content="">
    <meta name="author" content="">
    
</head>
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-grid.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/import.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/material-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('font-awesome/css/font-awesome.css')}}">
<style type="text/css">
body{
    background: #f2f2f2;
}
</style>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<body>
    <div class="form-login-app">
        <div class="text-center mb-5">
            <img src="{{asset('img/forest.png')}}" style="max-height: 110px">
            <div class="font-34 text-bold color-app">TripAceh</div>
        </div>
        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="m-0 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="mb-5">
                    <input id="name" type="text" class="form-control form-login" placeholder="Nama" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="m-0 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="mb-5">
                    <input id="email" type="email" class="form-control form-login" placeholder="Email" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="m-0 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="mb-5">
                    <input id="password" type="password" class="form-control form-login" placeholder="Password" name="password" required>

                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="m-0 form-group">
                <div class="mb-5">
                    <input id="password-confirm" type="password" class="form-control form-login" placeholder="Konfirmasi Password" name="password_confirmation" required>
                </div>
            </div>

            <div class="m-0 form-group">
                    <button type="submit" class="btn-app-login col-12 p-3">
                        Register
                    </button>
            </div>
            <div class="text-center mt-3">
                    <a href="{{url('/login')}}">Sudah punya akun ?</a>
                </div>
        </form>     
    </div>
</body>
</html>