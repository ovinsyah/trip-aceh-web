<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkomodasiWisatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akomodasi_wisatas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('akomodasi_id');
            $table->unsignedInteger('wisata_id');
            $table->timestamps();

            $table->foreign('akomodasi_id')
            ->references('id')
            ->on('akomodasis')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('wisata_id')
            ->references('id')
            ->on('wisatas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akomodasi_wisatas');
    }
}
