var file_imginput = [];
function uploadimage(event) {
    var output = document.getElementById('imgview');
    output.src = URL.createObjectURL(event.target.files[0]);
    getBase64(event.target.files[0]);
};

function getBase64(file) {
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
	 file_imginput = reader.result;
   };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}
var statusForm = 0;
var statusEdit = 0;
$(document).ready( function () {
	$('#formAdd').on('keyup change click',function () {
		statusForm = 1;
		statusEdit = 1;
		$('#judul,#deskripsi').removeClass('form-error');
		if($('#judul').val() == ''){
			$('#judul').addClass('form-error');
			statusForm = 0;
		}
		if($('#deskripsi').val() == ''){
			$('#deskripsi').addClass('form-error');
			statusForm = 0;
		}
		if($('#imginput').val() == ''){
			statusForm = 0;
		}
	});
});
